# Описание организации
Организация осуществляет подготовку бакалавров, магистров и аспирантов по ряду различных направлений. Имеется несколько филиалов, расположенных в разных городах. Помимо образовательной деятельности, также осуществляется и научная.
# Описание области автоматизации
Система должна позволять автоматизировать множество жизненных циклов электронных документов, включающих процессы создания, согласования, принятия решения и передаче на хранение.