package interfaces;

import model.KeysPair;

import java.util.UUID;

public interface KeyInterface {
    KeysPair addKeyPair(KeysPair keys);
    KeysPair getById(UUID k_id, UUID u_id);
    void updateKeyPair(KeysPair key);
}
