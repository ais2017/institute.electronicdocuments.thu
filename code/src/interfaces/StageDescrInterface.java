package interfaces;

import model.StageDescription;

import java.util.UUID;

public interface StageDescrInterface {
    public StageDescription getById(UUID sd_id);
    public StageDescription addStageDescr(StageDescription sd);
}
