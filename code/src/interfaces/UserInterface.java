package interfaces;

import javafx.util.Pair;
import model.DocTemplate;
import model.Document;
import model.KeysPair;
import model.User;

import javax.swing.*;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface UserInterface {
    User getById(UUID id);
    List<User> getUsersByType(int userType);
    List<User> getUsersWithoutActualKeyPair();
    int[] getUserTypes();
    List<User> getSignersByType(int userType);
}
