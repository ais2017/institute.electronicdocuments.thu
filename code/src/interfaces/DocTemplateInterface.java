package interfaces;

import javafx.util.Pair;
import model.DocTemplate;
import model.Document;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface DocTemplateInterface {
    DocTemplate getById(UUID id);
    DocTemplate addDocTemplate(DocTemplate t);
    DocTemplate updateTemplate(DocTemplate t, UUID id);
    Pair<UUID, DocTemplate> getByName(String name);
    Map<UUID, DocTemplate> getTemplatesForUser(int userType);
    Map<UUID, DocTemplate> getActualTemplatesForUser(int userType);
    Map<UUID, DocTemplate> getAllTemplates();
}
