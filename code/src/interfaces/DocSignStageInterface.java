package interfaces;

import model.DocSignStage;

import java.util.UUID;

public interface DocSignStageInterface {
    public DocSignStage getById(UUID id);
    public DocSignStage addDocSignStage(DocSignStage stage);
    public void updateSignStage(UUID uId, UUID docId, DocSignStage stage);
}
