package interfaces;

import javafx.util.Pair;
import model.Document;

import javax.swing.*;
import java.util.UUID;
import java.util.List;

public interface NotificationInterface {
    void sendNotificationToSigners(UUID uId, UUID docId, UUID[] users);
    void sendNotificationToCreatorAboutDocStatusChange(UUID userId, UUID docId);
    UUID[] getNotificationForDocCreator(UUID uId);
    List<Pair<UUID, UUID>> getNotificationForSigner(UUID uId);
}
