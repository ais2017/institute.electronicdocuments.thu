package interfaces;

import javafx.util.Pair;
import model.DocTemplate;
import model.Document;

import java.util.*;

public interface DocumentInterface {
    Document updateDocument(UUID docId, UUID uId, Document doc);
    Document getDocumentById(UUID uId, UUID docId);
    Map<UUID, Document> getAllDocumentsForUser(UUID id);
    Map<UUID, Document> getSignedDocumentsForUser(UUID id);
    Map<UUID, Document>getRefusedDocumentsForUser(UUID id);
    Map<UUID, Document> getNotSignedDocumentsForUser(UUID id);
}
