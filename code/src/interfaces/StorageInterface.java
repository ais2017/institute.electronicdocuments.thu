package interfaces;

import model.Document;

public interface StorageInterface {
    public void saveToStorage(Document doc) throws Exception;
}
