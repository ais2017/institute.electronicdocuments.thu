package interfaces;

import model.DocSign;

import java.util.UUID;

public interface DocSignInterface {
    public DocSign getById(UUID id);
    public DocSign addDocSign(UUID userId, UUID docId, UUID signerId, DocSign sign);
}
