package test_int;

import interfaces.NotificationInterface;
import javafx.util.Pair;

import java.util.*;

public class NotificationTestInt implements NotificationInterface {

    private Map<UUID, Pair<UUID, UUID>> signerNotif = new HashMap<>();
    private Map<UUID, UUID> creatorNotif = new HashMap<>();

    public void sendNotificationToSigners(UUID uId, UUID docId, UUID[] users) {
        if(users != null && users.length != 0) {
            for(int i = 0; i < users.length; ++i) {
                signerNotif.put(users[i], new Pair<UUID, UUID>(uId, docId));
            }
        }
    }

    public void sendNotificationToCreatorAboutDocStatusChange(UUID userId, UUID docId) {
        creatorNotif.put(userId, docId);
    }

    public UUID[] getNotificationForDocCreator(UUID uId) {
        if(!creatorNotif.containsKey(uId))
            return null;
        UUID[] docIds = {creatorNotif.get(uId)};
        return docIds;
    }

    public List<Pair<UUID, UUID>> getNotificationForSigner(UUID uId) {
        if (!signerNotif.containsKey(uId))
            return null;
        //System.out.println(uId);
        List<Pair<UUID, UUID>> notif = new ArrayList<>();
        notif.add(signerNotif.get(uId));
        //System.out.println(signerNotif.get(uId).getKey());
        return notif;
    }
}
