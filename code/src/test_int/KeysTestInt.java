package test_int;

import interfaces.KeyInterface;
import model.KeysPair;
import model.User;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class KeysTestInt implements KeyInterface {

    private List<KeysPair> keys = new ArrayList<>();

    public KeysPair addKeyPair(KeysPair key) {
        keys.add(key);
        return key;
    }

    public void updateKeyPair(KeysPair key) {
        for (int i = 0; i < keys.size(); ++i) {
            if(keys.get(i).getId().equals(key.getId()))
                keys.set(i, key);
        }
    }

    public KeysPair getById(UUID k_id, UUID u_id) {
        for(int i = 0; i < keys.size(); ++i) {
            if(keys.get(i).getId().equals(k_id) && keys.get(i).getOwner().getId().equals(u_id))
                return keys.get(i);
        }
        return null;
    }

    public KeysPair getLastKeyPair() {
        if(keys == null || keys.isEmpty())
            return null;
        return keys.get(keys.size()-1);
    }
}
