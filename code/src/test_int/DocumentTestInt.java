package test_int;

import interfaces.DocumentInterface;
import model.*;
import java.util.*;

public class DocumentTestInt implements DocumentInterface
 {
     public static Map<UUID, Document> documents = new HashMap<>();

    public DocumentTestInt(User user1, UUID u2, UUID u3){
        UUID[] signersId = {u2,u3};
            StageDescription last = new StageDescription(null, WayOfStagePassing.ANY, signersId, SignerRole.ABSOLUTE, UUID.randomUUID());
            StageDescription first = new StageDescription(last, WayOfStagePassing.ALL, SignerRole.RELATIVE, UUID.randomUUID());
            List<StageDescription> stages = new ArrayList<StageDescription>();
            stages.add(first);
            stages.add(last);
            DocSignStage stage1 = new DocSignStage(null, last, UUID.randomUUID());
            DocSignStage stage0 = new DocSignStage(stage1, first, UUID.randomUUID());
            List<DocSignStage> stages1 = new ArrayList<DocSignStage>();
            stages1.add(stage0);
            stages1.add(stage1);
            byte[] tt = new byte[]{(byte) 0x65, (byte) 0x37};
            DocTemplate t = new DocTemplate(tt, user1.getUserType(), "Template 1", stages);
            Document doc = new Document("Document", tt, null, t, user1, stages1, Status.PREPARING);
            UUID id = UUID.randomUUID();
            documents.put(id, doc);
    }

    public Document updateDocument(UUID docId, UUID uId, Document doc) {
        documents.put(docId, doc);
        return doc;
    }

     public Document getDocumentById(UUID uId, UUID docId) {
        return documents.get(docId);
     }

     public Map<UUID, Document> getAllDocumentsForUser(UUID id) {
        Map<UUID, Document> docs = new HashMap<>();
        for(Map.Entry<UUID, Document> entry: documents.entrySet()) {
            if(entry.getValue().getCreator().getId().equals(id))
                docs.put(entry.getKey(), entry.getValue());
        }
        return docs;
    }

    public Map<UUID, Document> getSignedDocumentsForUser(UUID id) {
        Map<UUID, Document> docs = new HashMap<>();
        for(Map.Entry<UUID, Document> entry: documents.entrySet()) {
            if(entry.getValue().getCreator().getId().equals(id) && entry.getValue().getStatus().equals(Status.SIGNED.getValue()))
                docs.put(entry.getKey(), entry.getValue());
        }
        return docs;
    }

    public Map<UUID, Document> getRefusedDocumentsForUser(UUID id) {
        Map<UUID, Document> docs = new HashMap<>();
        for(Map.Entry<UUID, Document> entry: documents.entrySet()) {
            if(entry.getValue().getCreator().getId().equals(id) && entry.getValue().getStatus().equals(Status.REFUSED.getValue()))
                docs.put(entry.getKey(), entry.getValue());
        }
        return docs;
    }

    public Map<UUID, Document> getNotSignedDocumentsForUser(UUID id) {
        Map<UUID, Document> docs = new HashMap<>();
        for(Map.Entry<UUID, Document> entry: documents.entrySet()) {
            if(entry.getValue().getCreator().getId().equals(id) && (entry.getValue().getStatus().equals(Status.PROCESSING.getValue()) || entry.getValue().getStatus().equals(Status.PREPARING.getValue())))
                docs.put(entry.getKey(), entry.getValue());
        }
        return docs;
    }
}

