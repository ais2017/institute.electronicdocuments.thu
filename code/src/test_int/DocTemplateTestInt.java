package test_int;

import interfaces.DocTemplateInterface;
import javafx.util.Pair;
import model.DocTemplate;
import model.SignerRole;
import model.StageDescription;
import model.WayOfStagePassing;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.print.Doc;
import java.util.*;

public class DocTemplateTestInt implements DocTemplateInterface
 {
     public static Map<UUID, DocTemplate> templates = new HashMap<>();

     public DocTemplateTestInt(UUID id1, UUID id2) {
         UUID[] signersId = {id1,id2};
         StageDescription last = new StageDescription(null, WayOfStagePassing.ANY, signersId, SignerRole.ABSOLUTE, UUID.randomUUID());
         StageDescription first = new StageDescription(last, WayOfStagePassing.ALL, SignerRole.RELATIVE, UUID.randomUUID());
         List<StageDescription> stages = new ArrayList<StageDescription>();
         stages.add(first);
         stages.add(last);
         byte[] tt = new byte[]{(byte) 0x65, (byte) 0x37};
         DocTemplate t = new DocTemplate(tt, 0, "Template 1", stages);
         templates.put(UUID.randomUUID(), t);
     }

    public DocTemplate getById(UUID id) {
        if(templates.containsKey(id))
                return templates.get(id);
        return null;
    }

    public DocTemplate addDocTemplate(DocTemplate t) throws IllegalArgumentException{
        for(Map.Entry<UUID, DocTemplate> entry: templates.entrySet()) {
            if(entry.getValue().getName().equals(t.getName()))
                throw new IllegalArgumentException("Error while saving template: template name must be unique!");
        }
        templates.put(UUID.randomUUID(), t);
        return t;
    }

    public DocTemplate updateTemplate(DocTemplate t, UUID tId) {
        if(templates.containsKey(tId)) {
            templates.put(tId, t);
            return t;
        }
        return null;
    }

    public Pair<UUID, DocTemplate> getByName(String name) throws IllegalArgumentException {
        for (Map.Entry<UUID, DocTemplate> entry : templates.entrySet()) {
            if(entry.getValue().getName().equals(name))
                return new Pair<UUID, DocTemplate>(entry.getKey(), entry.getValue());
        }
        throw new IllegalArgumentException("Template with name "+name+" not found!");
    }

    public Map<UUID, DocTemplate> getTemplatesForUser(int userType) {
        Map<UUID, DocTemplate> t = new HashMap<>();
        for(Map.Entry<UUID, DocTemplate> entry : templates.entrySet()) {
            if(entry.getValue().getUserType() == userType)
                t.put(entry.getKey(), entry.getValue());
        }
        return t;
    }

     public Map<UUID, DocTemplate> getActualTemplatesForUser(int userType) {
         Map<UUID, DocTemplate> t = new HashMap<>();
         for(Map.Entry<UUID, DocTemplate> entry : templates.entrySet()) {
             if(entry.getValue().getUserType() == userType && entry.getValue().checkIfActual())
                 t.put(entry.getKey(), entry.getValue());
         }
         return t;
     }

    public Map<UUID, DocTemplate> getAllTemplates() {
        return templates;
    }
}
