package test_int;

import interfaces.DocSignInterface;
import model.DocSign;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DocSignTestInt implements DocSignInterface {
    List<DocSign> signs = new ArrayList<DocSign>();

    public DocSign getById(UUID id) {
        for(int i = 0; i < signs.size(); ++i) {
            if(signs.get(i).getId() == id)
                return signs.get(i);
        }
        return null;
    }
    public DocSign addDocSign(UUID uId, UUID docId, UUID signerId, DocSign sign) {
        if(getById(sign.getId()) == null) {
            signs.add(sign);
            return sign;
        }
        return null;
    }
}
