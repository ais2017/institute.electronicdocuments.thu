package test_int;

import interfaces.StorageInterface;
import model.Document;

import java.util.ArrayList;
import java.util.List;

public class StorageTestInt implements StorageInterface {
    List<Document> docs;

    public StorageTestInt() {
        docs = new ArrayList<Document>();
    }

    public void saveToStorage(Document doc) {
        docs.add(doc);
    }
}
