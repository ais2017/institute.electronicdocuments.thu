package test_int;

import interfaces.UserInterface;
import model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserTestInt implements UserInterface {

    private List<User> users;

    public UserTestInt(){
        users = new ArrayList<>();
        User user1 = new User(0, null, UUID.randomUUID(), null, "Tom");
        User user2 = new User(1, user1, UUID.randomUUID(), null, "Nick");
        User user3 = new User(2, user1, UUID.randomUUID(), null, "Liz");
        User user4 = new User(3, user3, UUID.randomUUID(), null, "Ann");
        users.add(user2);
        users.add(user1);;
        users.add(user3);
        users.add(user4);
    }

    public User getById(UUID id){
        List<User> list = new ArrayList<>();
        for(int i = 0; i < users.size(); ++i) {
            if(users.get(i).getId().equals(id))
              return users.get(i);
        }
        return null;
    }

    public List<User> getUsersWithoutActualKeyPair() {
        List<User> listOfUsers = new ArrayList<User>();
        for(int i = 0; i < users.size(); ++i) {
            if(users.get(i).getActualKeyPair() == null)
                listOfUsers.add(users.get(i));
        }
        return listOfUsers;
    }

    public List<User> getUsersByType(int userType) {
        List<User> listOfUsers = new ArrayList<User>();
        for(int i = 0; i < users.size(); ++i) {
            if (users.get(i).getUserType() == userType)
                listOfUsers.add(users.get(i));
        }
        return listOfUsers;
    }

    public int[] getUserTypes() {
        int[] signers = {0, 1, 2, 3};
        return signers;
    }

    public List<User> getSignersByType(int userType){
        List<User> listOfUsers = new ArrayList<User>();
        for(int i = 0; i < users.size(); ++i) {
            if (users.get(i).getUserType() == userType)
                listOfUsers.add(users.get(i));
        }
        return listOfUsers;
    }
}
