package test_int;

import interfaces.StageDescrInterface;
import model.StageDescription;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class StageDescrTestInt implements StageDescrInterface
 {
    List<StageDescription> st_d = new ArrayList<StageDescription>();

    public StageDescription getById(UUID sd_id) {
        for(int i = 0; i < st_d.size(); ++i) {
            if(st_d.get(i).getId() == sd_id)
                return st_d.get(i);
        }
        return null;
    }

    public StageDescription addStageDescr(StageDescription sd) {
        for(int i = 0; i < st_d.size(); ++i) {
            if(st_d.get(i).getId() == sd.getId())
                return null;
        }
        st_d.add(sd);
        return null;
    }
}
