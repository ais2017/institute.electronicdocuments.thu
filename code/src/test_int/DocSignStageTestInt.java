package test_int;

import interfaces.DocSignStageInterface;
import model.DocSignStage;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DocSignStageTestInt implements DocSignStageInterface {
    List<DocSignStage> stages = new ArrayList<DocSignStage>();

    public DocSignStage getById(UUID id) {
        for(int i = 0; i < stages.size(); ++i) {
            if(stages.get(i).getId() == id)
                return stages.get(i);
        }
        return null;
    }
    public DocSignStage addDocSignStage(DocSignStage stage) {
        if(getById(stage.getId()) == null) {
            stages.add(stage);
            return stage;
        }
        return null;
    }

    public void updateSignStage(UUID uId, UUID docId, DocSignStage stage) {

    }
}
