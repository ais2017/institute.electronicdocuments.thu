package model;

import java.security.SignatureException;

public enum SignerRole {
    ABSOLUTE("ABSOLUTE"),
    RELATIVE("RELATIVE");

    String value;
    SignerRole (String r) { value = r;};

    public String getValue() {
        return value;
    }
}
