package model;

public enum WayOfStagePassing {
    ALL("ALL"),
    ANY("ANY");
    protected String value;
    WayOfStagePassing (String w) {
        value = w;
    }

    public String getValue() {
        return value;
    }
}
