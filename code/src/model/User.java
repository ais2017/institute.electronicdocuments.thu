package model;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.*;

/**
 *
 * @author 1
 */

public class User {
    private User master;
    private int userType;
    private List <KeysPair> keys;
    private UUID id;
    private String name;

    public User(int type, User m, UUID uuid, List<KeysPair> k, String userName) throws IllegalArgumentException {
        if(uuid == null)
            throw new IllegalArgumentException("Empty uuid!");
        if(k == null) {
            keys = new ArrayList<KeysPair>();
        }
        else {
            String error = checkKeySet(k, uuid);
            if(error != null)
                throw new IllegalArgumentException(error);
            keys = new ArrayList<KeysPair>(k);
        }
        if(userName == null || userName.isEmpty())
            throw new IllegalArgumentException("Empty user name!");
        master = m;
        userType = type;
        id = uuid;
    }

    private String checkKeySet(List<KeysPair> k, UUID uuid) {
        int cntActualKey = 0;
        for (int i = 0; i < k.size(); ++i) {
            if (k.get(i).getOwner().getId() != uuid)
                return "Keys and user don`t match!";
            if (k.get(i).getActualFlag())
                ++cntActualKey;
        }
        if(cntActualKey > 1)
            return "More than 1 actual keys!";
        return null;
    }

    public String getName() {
        return name;
    }

    public void setKeys(List<KeysPair> k) throws IllegalArgumentException {
        if(k != null && !k.isEmpty()) {
            String error = checkKeySet(k, id);
            if(error != null)
                throw new IllegalArgumentException(error);
            keys = new ArrayList<KeysPair>(k);
        }
    }

    public List <KeysPair> getKeys() {
        return keys;
    }

    public UUID getId() {
        return id;
    }

    public User getMaster() {
        return master;
    }

    public int getUserType() {
        return userType;
    }

    public KeysPair getActualKeyPair(Calendar date) throws IllegalStateException {
        for(int i = 0; i < keys.size(); ++i) {
            if (keys.get(i).checkIfActual(date))
                return keys.get(i);
        }
        return null;
    }

    public KeysPair getKeyPairForDate(Calendar date) throws IllegalStateException {
        Calendar d = Calendar.getInstance();
        d.setTime(new Date(Long.MIN_VALUE));
        KeysPair k = null;
        for(int i = 0; i < keys.size(); ++i) {
            if (keys.get(i).checkKeyDates(date) && d.compareTo(keys.get(i).getStartDate()) < 0) {
                d = keys.get(i).getStartDate();
                k = keys.get(i);
            }
        }
        return k;
    }

    public KeysPair getActualKeyPair() throws IllegalStateException {
        Calendar date = Calendar.getInstance();
        return getActualKeyPair(date);
    }

    public KeysPair generateKeyPair(UUID k_id) throws NoSuchAlgorithmException, ParseException {
        KeysPair act_k = getActualKeyPair();
        if(act_k != null) {
            act_k.setNotActual();
            //throw new IllegalStateException("The user id="+id+" has actual key pair!");
        }
        KeysPair k = new KeysPair(this, k_id);
        keys.add(k);
        return k;
    }

}


