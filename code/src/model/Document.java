package model;

import Exceptions.IllegalStageSignException;
import Exceptions.IllegalUserException;
import Exceptions.NoActualKeyPairException;
import Exceptions.UserAlreadySignedException;

import javax.swing.plaf.synth.SynthTabbedPaneUI;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.*;

/**
 * @author 1
 */
public class Document {
    String name;
    byte[] document;
    Calendar date;
    DocTemplate template;
    User creator;
    List<DocSignStage> stages;
    int currentStage;
    Status status;

    public Document(String n, byte[] doc, Calendar dd, DocTemplate t, User u, List<DocSignStage> st, Status stat) throws IllegalArgumentException {
        if(n == null || n.isEmpty())
            throw new IllegalArgumentException("Empty document name!");
        if(doc == null || doc.length == 0)
            throw new IllegalArgumentException("Empty document content!");
        if(dd == null && (stat.getValue().equals(Status.SIGNED.getValue()) || stat.getValue().equals(Status.REFUSED.getValue())))
            throw new IllegalArgumentException("Empty date of document pass!");
        if(u == null)
            throw new IllegalArgumentException("Empty creator!");
        if(t == null)
            throw new IllegalArgumentException("No template!");
        if(st == null || st.isEmpty())
            throw new IllegalArgumentException("No stages!");
        if(stat == null)
            throw new IllegalArgumentException("Empty status!");
        if(t.getUserType() != u.getUserType())
            throw new IllegalArgumentException("Illegal template for user document!");
        if(st.size() != t.getDescriptions().size())
            throw new IllegalArgumentException("Stages size and description size don`t match!");
        if(st.get(st.size()-1).getNextStage() != null)
            throw new IllegalArgumentException("No last stage!");
        int cntRefusedStages = 0;
        for(int i = 0; i < st.size()-1; ++i) {
            if(!st.get(i).getDescription().equals(t.getDescriptions().get(i)))
                throw new IllegalArgumentException("Stage " + i + " and description " + i + " don`t match!");
            if(!st.get(i).getNextStage().equals(st.get(i+1)))
                throw new IllegalArgumentException("Illegal next stage for "+i+" stage!");
            if(st.get(i).getStatus().equals(Status.PROCESSING.getValue()) && !st.get(i+1).getStatus().equals(Status.PROCESSING.getValue()))
                throw new IllegalArgumentException("Stage "+i+" is not passed, but next stage is passed!");
            if(st.get(i).getStatus().equals(Status.REFUSED.getValue()) && !st.get(i+1).getStatus().equals(Status.PROCESSING.getValue()))
                throw new IllegalArgumentException("Stage "+i+" is refused and next stage is refused or signed!");
            if(stat.getValue().equals(Status.PROCESSING.getValue()) && (st.get(st.size()-1).equals(Status.SIGNED.getValue()) || st.get(i).getStatus().equals(Status.REFUSED.getValue())))
                throw new IllegalArgumentException("Illegal status: processing!");
            if(stat.getValue().equals(Status.PREPARING.getValue()) && !st.get(i).getStatus().equals(Status.PROCESSING.getValue()))
              throw new IllegalArgumentException("Illegal status: preparing!");
            if(st.get(i).getStatus().equals(Status.REFUSED.getValue()))
                ++cntRefusedStages;
        }
        if(stat.getValue().equals(Status.REFUSED.getValue()) && cntRefusedStages == 0)
            throw new IllegalArgumentException("Illegal status: refused!");
        if(stat.getValue().equals(Status.SIGNED.getValue()) && !st.get(st.size()-1).getStatus().equals(Status.SIGNED.getValue()))
            throw new IllegalArgumentException("Illegal status: signed");
        name = n;
        document = doc;
        if(stat.getValue().equals(Status.SIGNED.getValue()) || stat.getValue().equals(Status.REFUSED.getValue()))
            date = dd;
        currentStage = st.size()-1;
        for(int i = 0; i < st.size(); ++i) {
            if(st.get(i).getStatus().equals(Status.PROCESSING.getValue()) || st.get(i).getStatus().equals(Status.REFUSED.getValue())) {
                currentStage = i;
                break;
            }
        }
        template = t;
        creator = u;
        status = stat;
        stages = new ArrayList<DocSignStage>(st);
    }

    public boolean setEnableToSign() {
        if(status.getValue().equals(Status.PREPARING.getValue())) {
            status = Status.PROCESSING;
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public byte[] getDoc() {
        return document;
    }

    public Calendar getDatePass() {
        return date;
    }

    public boolean checkIfReady() {
        if(status.getValue().equals(Status.PROCESSING.getValue()) || status.getValue().equals(Status.PREPARING.getValue()))
            return false;
        return true;
    }

    public String getCommentOfLastSign() {
        List <DocSign> s = stages.get(currentStage).getSigns();
        if (s.isEmpty())
            return null;
        return s.get(s.size()-1).getComment();
    }

    public UUID[] getSignersIdOfCurrentStage() {
        if(stages.get(currentStage).getDescription().getRole().equals(SignerRole.ABSOLUTE.getValue()))
            return stages.get(currentStage).getDescription().getSigners();
        UUID[] signer = {creator.getMaster().getId()};
        return signer;
    }

    public String getStatus() {
        return status.getValue();
    }

    public DocTemplate getTemplate() {
        return template;
    }

    public User getCreator() {
        return creator;
    }

    public DocSignStage getCurrentStage() {
        return stages.get(currentStage);
    }

    public int getCurrentStageIndex() {
        return currentStage;
    }

    private void updateCurrentStage() {
        if(currentStage != stages.size()-1 && stages.get(currentStage).getStatus().equals(Status.SIGNED.getValue())) {
            ++currentStage;
            return;
        }
        if(stages.get(currentStage).getStatus().equals(Status.REFUSED.getValue()))
            status = Status.REFUSED;
        else if(currentStage == stages.size()-1 && stages.get(currentStage).getStatus().equals(Status.SIGNED.getValue()))
            status = Status.SIGNED;
        if(status.getValue().equals(Status.REFUSED.getValue()) || status.getValue().equals(Status.SIGNED.getValue())) {
            Calendar passDate = Calendar.getInstance();
            date = passDate;
        }
    }

    public DocSignStage getPreviousStage() {
        if(currentStage != 0)
            return stages.get(currentStage-1);
        return null;
    }

    public DocSign signDocument(String s, User signer, UUID signId) throws IllegalStateException, IllegalStageSignException, UserAlreadySignedException, IllegalUserException, NoActualKeyPairException, NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        if(status.getValue().equals(Status.SIGNED.getValue()) || status.getValue().equals(Status.REFUSED.getValue()))
            throw new IllegalStateException("This document is already signed or refused!");
        if(status.getValue().equals(Status.PREPARING.getValue()))
            throw new IllegalStateException("The document is at preparing stage!");
        DocSign sign = stages.get(currentStage).makeSign(s, creator, signer, document, signId);
        updateCurrentStage();
        return sign;
    }

    public DocSign refuseToSign(String s, String comment, User signer, UUID signId) throws IllegalStateException, IllegalStageSignException, UserAlreadySignedException, IllegalUserException, NoActualKeyPairException{
        if(status.getValue().equals(Status.SIGNED.getValue()) || status.getValue().equals(Status.REFUSED.getValue()))
            throw new IllegalStateException("This document is already signed or refused!");
        if(status.getValue().equals(Status.PREPARING.getValue()))
            throw new IllegalStateException("The document is at preparing stage!");
        DocSign sign = stages.get(currentStage).refuseSign(s, comment, creator, signer, signId);
        updateCurrentStage();
        return sign;
    }
}
