package model;

public enum Status {
    PROCESSING("PROCESSING"),
    SIGNED("SIGNED"),
    REFUSED("REFUSED"),
    PREPARING("PREPARING");
    String value;
    Status (String st) {
        value = st;
    }

    public String getValue() {
        return value;
    }
}
