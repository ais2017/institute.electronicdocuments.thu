package model;

import Exceptions.NoActualKeyPairException;

import java.util.*;
import java.security.*;

public class DocSign {
    private String status;
    private String comment;
    private Calendar date;
    private boolean refused;
    private byte[] sign;
    private KeysPair keys;
    private UUID id;

    public DocSign(String s, String c, Calendar d, boolean r, byte[] sn, KeysPair k, UUID uuid) throws IllegalArgumentException {
        if(s == null || s.isEmpty())
            throw new IllegalArgumentException("Empty status!");
        if(d == null)
            throw new IllegalArgumentException("Empty date!");
        if(sn == null || sn.length == 0)
            throw new IllegalArgumentException("Empty sign content!");
        if(k == null)
            throw new IllegalArgumentException("Empty key pair!");
        if(!k.checkKeyDates(d))
            throw new IllegalArgumentException("Key pair is not valid at date "+d.getTime()+"!");
        if(uuid == null)
            throw new IllegalArgumentException("Empty uuid!");
        if(r && (c == null || c.isEmpty()))
            throw new IllegalArgumentException("Comment not specified!");
        status = s;
        comment = c;
        sign = sn;
        date = d;
        keys = k;
        refused = r;
        id = uuid;
    }
    //Sign a document
    public DocSign(String s, byte[] doc, KeysPair k, UUID uuid) throws InvalidKeyException, NoSuchAlgorithmException, SignatureException, NoActualKeyPairException, IllegalArgumentException {
        if(s == null || s.isEmpty())
            throw new IllegalArgumentException("Empty status!");
        if(doc == null || doc.length == 0)
            throw new IllegalArgumentException("Empty document content!");
        if(k == null)
            throw new IllegalArgumentException("Empty key pair!");
        if(uuid == null)
            throw new IllegalArgumentException("Empty uuid!");
        Calendar d = Calendar.getInstance();
        if(!k.checkIfActual(d))
            throw new NoActualKeyPairException("Key pair is not valid!");
        status = s;
        keys = k;
        Signature rsa = Signature.getInstance("SHA1withRSA");
        rsa.initSign(keys.getPrivateKey());
        rsa.update(doc);
        sign = rsa.sign();
        date = d;
        refused = false;
        id = uuid;
    }

    public UUID getId() {
        return id;
    }
    // refuse to sign a document
    public DocSign(String s, String c, KeysPair k, UUID uuid) throws IllegalArgumentException, NoActualKeyPairException {
        if(s.isEmpty())
            throw new IllegalArgumentException("Empty status!");
        if(c.isEmpty())
            throw new IllegalArgumentException("Empty comment!");
        if(k == null)
            throw new IllegalArgumentException("Empty key pair!");
        if(uuid == null)
            throw new IllegalArgumentException("Empty uuid!");
        Calendar d = Calendar.getInstance();
        if(!k.checkIfActual(d))
            throw new NoActualKeyPairException("Key pair is not valid!");
        refused = true;
        date = d;
        keys = k;
        status = s;
        comment = c;
        id = uuid;
    }

    public String getStatus() {
        return status;
    }

    public String getComment() {
        return comment;
    }

    public Calendar getDateOfSign() {
        return date;
    }

    public boolean checkIfRefused() {
        return refused;
    }

    public boolean checkSign(byte[] doc) throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        if(refused)
            return false;
        Signature rsa = Signature.getInstance("SHA1withRSA");
        rsa.initVerify(keys.getPublicKey());
        rsa.update(doc);
        return rsa.verify(sign);
    }

    public User getSigner() {
        return keys.getOwner();
    }

}
