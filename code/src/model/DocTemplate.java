package model;

import java.util.*;

/**
 *
 * @author 1
 */
public class DocTemplate {
    private byte[] template;
    private int userType;
    private String name;
    private Calendar date;
    private boolean isActual;
    private List<StageDescription> descriptions;

    public DocTemplate(byte[] t, int u, String n, List<StageDescription> s, Calendar d, boolean actual) throws IllegalArgumentException{
        if(t == null || t.length == 0)
            throw new IllegalArgumentException("Empty template!");
        if(n == null || n.isEmpty())
            throw new IllegalArgumentException("Empty template name!");
        if(s == null || s.isEmpty())
            throw new IllegalArgumentException("Empty stage description!");
        if(!s.get(s.size()-1).checkIfLast())
            throw new IllegalArgumentException("No last stage!");
        if(d == null)
            throw new IllegalArgumentException("Empty date!");
        for(int i = 0; i < s.size()-1; ++i) {
            if(s.get(i).getNextStageDescription() == null || !s.get(i).getNextStageDescription().equals(s.get(i+1)))
                throw new IllegalArgumentException("Illegal stages! "+i);
        }
        template = t;
        userType = u;
        name = n;
        date = d;
        descriptions = s;
        isActual = actual;
    }

    public DocTemplate(byte[] t, int u, String n, List<StageDescription> s) throws IllegalArgumentException{
        if(t.length == 0)
            throw new IllegalArgumentException("Empty template!");
        if(s.isEmpty())
            throw new IllegalArgumentException("Empty stage description!");
        if(n.isEmpty())
            throw new IllegalArgumentException("Empty template name!");
        if(!s.get(s.size()-1).checkIfLast())
            throw new IllegalArgumentException("No last stage!");
        for(int i = 0; i < s.size()-1; ++i) {
            if(s.get(i).getNextStageDescription() == null || !s.get(i).getNextStageDescription().equals(s.get(i+1)))
                throw new IllegalArgumentException("Illegal stages! "+i);
        }
        template = t;
        userType = u;
        name = n;
        date = Calendar.getInstance();
        descriptions = s;
        isActual = true;
    }

    public void setNotActual() {
        isActual = false;
    }

    public boolean checkIfActual() { return isActual; }

    public byte[] getTemplate() {
        return template;
    }

    public int getUserType() {
        return userType;
    }

    public String getName() {
        return name;
    }

    public Calendar getCreationDate() {
        return date;
    }

    public List<StageDescription> getDescriptions() {
        return descriptions;
    }
}

