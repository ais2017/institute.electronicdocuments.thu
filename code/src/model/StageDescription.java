package model;

import interfaces.StageDescrInterface;

import java.util.UUID;

public class StageDescription {
    private StageDescription descrOfNextStage;
    private WayOfStagePassing wayOfPass;
    private UUID[] signers;
    private SignerRole role;
    private UUID uuid;

    public StageDescription(StageDescription d, WayOfStagePassing w, UUID[] s, SignerRole role, UUID id) throws IllegalArgumentException {
        if(w == null)
            throw new IllegalArgumentException("Empty way of pass!");
        if(s == null || s.length == 0)
            throw new IllegalArgumentException("Empty signers!");
        if(role == null)
            throw new IllegalArgumentException("Empty signer role!");
        if(!role.getValue().equals(SignerRole.ABSOLUTE.getValue()))
            throw new IllegalArgumentException("You should not specify signers uuid!");
        if(id == null)
            throw new IllegalArgumentException("Empty uuid!");
        descrOfNextStage = d;
        wayOfPass = w;
        signers = s;
        this.role = role;
        uuid = id;
    }

    public UUID getId() {
        return uuid;
    }

    public StageDescription(StageDescription d, WayOfStagePassing w, SignerRole role, UUID id) throws IllegalArgumentException {
        if(w == null)
            throw new IllegalArgumentException("Empty way of pass!");
        if(role == null)
            throw new IllegalArgumentException("Empty signer role!");
        if(!role.getValue().equals(SignerRole.RELATIVE.getValue()))
            throw new IllegalArgumentException("Empty signers uuid!");
        if(id == null)
            throw new IllegalArgumentException("Empty uuid!");
        uuid = id;
        descrOfNextStage = d;
        wayOfPass = w;
        this.role = role;
    }

    public boolean equals(StageDescription st) {
        if(!st.getWayPass().equals(wayOfPass.getValue()) || !st.getRole().equals(role.getValue()) || (st.getNextStageDescription() == null && descrOfNextStage != null) || (st.getNextStageDescription() == null && descrOfNextStage != null) || !uuid.equals(st.getId()))
            return  false;
        if(st.getSigners() == null && signers != null || signers == null && st.getSigners() != null)
            return false;
        if(signers != null && st.getSigners() != null) {
            if(signers.length != st.getSigners().length)
                return false;
            for (int i = 0; i < signers.length; ++i) {
                if (signers[i] != st.getSigners()[i])
                    return false;
            }
        }
        if(st.getNextStageDescription() != null && descrOfNextStage != null && !st.getNextStageDescription().getId().equals(descrOfNextStage.getId()))
            return false;
        return true;
    }

    public String getWayPass() {
        return wayOfPass.getValue();
    }

    public UUID[] getSigners() {
        return signers;
    }

    public String getRole() {
        return role.getValue();
    }

    public StageDescription getNextStageDescription() {
        return descrOfNextStage;
    }

    public boolean checkIfLast() {
        if(descrOfNextStage == null)
            return true;
        return false;
    }
}
