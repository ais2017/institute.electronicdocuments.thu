package model;

import java.security.*;
import java.text.ParseException;
import java.util.*;
/**
 *
 * @author 1
 */
public class KeysPair {
    private User owner;
    private KeyPair keys;
    private boolean isActual;
    private Calendar startDate;
    private Calendar endDate;
    private UUID uuid;

    public KeysPair(User u, UUID id) throws NoSuchAlgorithmException, ParseException, IllegalArgumentException {
        if(u == null)
            throw new IllegalArgumentException("Empty owner!");
        if(id == null)
            throw new IllegalArgumentException("Empty uuid!");
        owner = u;
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(512);
        keys = keyGen.generateKeyPair();
        startDate = Calendar.getInstance();
        endDate = Calendar.getInstance(startDate.getTimeZone());
        endDate.setTime(startDate.getTime());
        endDate.add(Calendar.YEAR, 3);
        uuid = id;
        isActual = true;
    }

    public KeysPair(User u, UUID id, Calendar std, Calendar endd, KeyPair k, boolean actual) throws IllegalArgumentException {
        if(u == null)
            throw new IllegalArgumentException("Empty owner!");
        if(id == null)
            throw new IllegalArgumentException("Empty uuid!");
        if(k == null)
            throw new IllegalArgumentException("Empty key!");
        if(std == null || endd == null)
            throw new IllegalArgumentException("Empty date!");
        if(std.compareTo(endd) > 0)
            throw new IllegalArgumentException("Startdate is more than enddate!");
        owner = u;
        keys = new KeyPair(k.getPublic(), k.getPrivate());
        startDate = std;
        endDate = endd;
        uuid = id;
        isActual = actual;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public UUID getId() {
        return uuid;
    }

    public PrivateKey getPrivateKey() {
        return keys.getPrivate();
    }

    public PublicKey getPublicKey() {
        return keys.getPublic();
    }

    public User getOwner() {
        return owner;
    }

    public boolean checkKeyDates(Calendar date) {
        if(date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0)
            return true;
        return false;
    }

    public boolean checkIfActual(Calendar date) throws IllegalStateException{
        if(date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0 && isActual)
            return true;
        else if(isActual)
            throw new IllegalStateException("The period of keys activation has passed, but flag isActual = true!");
        return false;
    }

    public void setNotActual() {
        isActual = false;
    }

    public boolean getActualFlag() {
        return isActual;
    }

}
