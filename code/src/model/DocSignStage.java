package model;

import Exceptions.IllegalStageSignException;
import Exceptions.IllegalUserException;
import Exceptions.NoActualKeyPairException;
import Exceptions.UserAlreadySignedException;

import javax.print.Doc;
import javax.print.DocFlavor;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.*;

/**
 *
 * @author 1
 */
public class DocSignStage {
    private DocSignStage nextStage;
    private Calendar dateOfSign;
    private List<DocSign> signs;
    private StageDescription descr;
    private Status status;
    private UUID id;

    public DocSignStage(DocSignStage n, Calendar d, List<DocSign> st, StageDescription sd, Status stat, UUID uuid, User owner) throws IllegalArgumentException {
        if(sd == null)
            throw new IllegalArgumentException("Empty stage description!");
        if(stat == null)
            throw new IllegalArgumentException("Empty status!");
        if(stat.getValue().equals(Status.PREPARING.getValue()))
            throw new IllegalArgumentException("Stage can`t be with PREPARING status!");
        if(d == null && !stat.getValue().equals(Status.PROCESSING.getValue()))
            throw new IllegalArgumentException("Empty date!");
        if(d != null && (stat.getValue().equals(Status.PROCESSING.getValue()) || stat.getValue().equals(Status.PREPARING.getValue())))
            throw new IllegalArgumentException("Date of document signment is not null, but document status is PREPARING or PROCESSING!");
        if(uuid == null)
            throw new IllegalArgumentException("Empty uuid!");
        if(owner == null)
            throw new IllegalArgumentException("Empty owner!");
        if (!stat.getValue().equals(Status.PROCESSING.getValue()) && (st == null || st.isEmpty()))
            throw new IllegalArgumentException("Empty list of signs!");
        if(sd.getRole().equals(SignerRole.ABSOLUTE.getValue())) {
            if(stat.getValue().equals(Status.SIGNED.getValue()) && (sd.getWayPass().equals(WayOfStagePassing.ALL.getValue()) && st.size() != sd.getSigners().length || sd.getWayPass().equals(WayOfStagePassing.ANY) && st.size() != 1))
                throw new IllegalArgumentException("Illegal status: signed!");
            else if(stat.getValue().equals(Status.REFUSED.getValue()) && !st.get(st.size()-1).checkIfRefused())
                throw new IllegalArgumentException("Illegal status: refused!");
            else if(stat.getValue().equals(Status.PROCESSING.getValue()) && st != null && st.size() != 0) {
                if(st.get(st.size()-1).checkIfRefused() || st.size() == sd.getSigners().length || sd.getWayPass().equals(WayOfStagePassing.ANY.getValue()))
                    throw new IllegalArgumentException("Illegal status: processing!");
            }
            if(st != null && st.size() != 0) {
                for(int i = 0; i < st.size(); ++i) {
                    if(!st.get(i).getSigner().getId().equals(sd.getSigners()[i]))
                        throw new IllegalArgumentException("UUID in stage and UUID in Description don`t match!");
                }
            }
        }
        else {
            if(stat.getValue().equals(Status.SIGNED.getValue()) && (st == null || st.size() != 1))
                throw new IllegalArgumentException("Illegal status: signed!");
            else if(stat.getValue().equals(Status.REFUSED.getValue()) && !st.get(0).checkIfRefused())
                throw new IllegalArgumentException("Illegal status: refused!");
            else if(stat.getValue().equals(Status.PROCESSING.getValue()) && st != null && st.size() != 0) {
                throw new IllegalArgumentException("Illegal status: processing!");
            }
            if(st != null && !st.isEmpty() && (owner.getMaster() == null || !st.get(0).getSigner().getId().equals(owner.getMaster().getId())))
                throw new IllegalArgumentException("Master id and signer id don`t match!");
        }
        nextStage = n;
        signs = new ArrayList<DocSign>(st);
        descr = sd;
        status = stat;
        id = uuid;
        dateOfSign = d;
    }

    public DocSignStage(DocSignStage n, StageDescription sd, UUID uuid) throws IllegalArgumentException {
        if(sd == null)
            throw new IllegalArgumentException("Empty description!");
        if(uuid == null)
            throw new IllegalArgumentException("Empty uuid!");
        nextStage = n;
        signs = new ArrayList<DocSign>();
        descr = sd;
        status = Status.PROCESSING;
        id = uuid;
    }

    public UUID getId() {
        return id;
    }

    public DocSignStage getNextStage() {
        return nextStage;
    }

    public Calendar getPassDate() {
        return dateOfSign;
    }

    public List<DocSign> getSigns() {
        return signs;
    }

    public StageDescription getDescription() {
        return descr;
    }

    public String getStatus() {
        return status.getValue();
    }

    public DocSign makeSign(String s, User owner, User signer, byte[] doc, UUID signId) throws IllegalStageSignException, UserAlreadySignedException, IllegalUserException, NoActualKeyPairException, NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        if(!status.getValue().equals(Status.PROCESSING.getValue()))
            throw new IllegalStageSignException("This stages is already passed!");
        if(owner == null)
            throw new IllegalUserException("Creator is empty!");
        if(signer == null)
            throw new IllegalStageSignException("Signer is empty!");
        if(checkIfUserSigned(signer))
            throw new UserAlreadySignedException("This user has already signed a document");
        if(descr.getRole().equals(SignerRole.ABSOLUTE.getValue())) {
            if (!checkIfUserCanSign(signer))
                throw new IllegalUserException("This user can`t sign this document!");
        }
        else {
            if (!checkIfUserCanSign(owner, signer))
                throw new IllegalUserException("This user can`t sign this document!");
        }
        if(signer.getActualKeyPair() == null)
            throw new NoActualKeyPairException("The user doesn`t have actual key pair!");
        DocSign sign = new DocSign(s, doc, signer.getActualKeyPair(), signId);
        signs.add(sign);
        updateStatus();
        return sign;
    }

    boolean checkIfUserCanSign(User s) {
        if(descr.getRole().equals(SignerRole.ABSOLUTE.getValue())) {
            for (int i = 0; i < descr.getSigners().length; ++i) {
                if (descr.getSigners()[i].equals(s.getId()))
                    return true;
            }
        }
        return false;
    }

    boolean checkIfUserCanSign(User owner, User s) {
        if(descr.getRole().equals(SignerRole.RELATIVE.getValue())) {
            if(owner.getMaster() == null)
                return false;
            if(owner.getMaster().getId().equals(s.getId()))
                return true;
        }
        return false;
    }

    void updateStatus() {
        if(!status.getValue().equals(Status.PROCESSING.getValue()))
            return;
        Calendar d = Calendar.getInstance();
        d.set(Calendar.HOUR_OF_DAY, 0);
        d.set(Calendar.MINUTE, 0);
        d.set(Calendar.SECOND, 0);
        d.set(Calendar.MILLISECOND, 0);
        if(signs.get(signs.size()-1).checkIfRefused()) {
            status = Status.REFUSED;
            dateOfSign = d;
            return;
        }
        if (descr.getWayPass().equals(WayOfStagePassing.ANY.getValue()) && signs.size() == 1) {
            status = Status.SIGNED;
            dateOfSign = d;
        }
        else if (descr.getWayPass().equals(WayOfStagePassing.ALL.getValue())) {
            if(descr.getRole().equals(SignerRole.ABSOLUTE.getValue()) && descr.getSigners().length == signs.size()) {
                status = Status.SIGNED;
                dateOfSign = d;
            }
            else if(descr.getRole().equals(SignerRole.RELATIVE.getValue()) && signs.size() == 1) {
                status = Status.SIGNED;
                dateOfSign = d;
            }
        }
    }

    public boolean checkIfUserSigned(User signer) {
        for(int i = 0; i < signs.size(); ++i) {
            if(signs.get(i).getSigner().getId().equals(signer.getId()))
                return true;
        }
        return false;
    }

    public DocSign refuseSign(String s, String comment, User owner, User signer, UUID signId) throws IllegalStageSignException, UserAlreadySignedException, IllegalUserException, NoActualKeyPairException{
        if(!status.getValue().equals(Status.PROCESSING.getValue()))
            throw new IllegalStageSignException("This stages is already passed!");
        if(owner == null)
            throw new IllegalUserException("Empty owner!");
        if(signer == null)
            throw new IllegalStageSignException("Signer is empty!");
        if(checkIfUserSigned(signer))
            throw new UserAlreadySignedException("This user has already signed a document");
        if(descr.getRole().equals(SignerRole.ABSOLUTE.getValue())) {
            if (!checkIfUserCanSign(signer))
                throw new IllegalUserException("This user can`t sign this document!");
        }
        else {
            if (!checkIfUserCanSign(owner, signer))
                throw new IllegalUserException("This user can`t sign this document!");
        }
        if(signer.getActualKeyPair() == null)
            throw new NoActualKeyPairException("The user doesn`t have actual key pair!");
        DocSign sign = new DocSign(s, comment, signer.getActualKeyPair(), signId);
        signs.add(sign);
        updateStatus();
        return sign;
    }


}

