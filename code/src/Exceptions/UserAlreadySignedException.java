package Exceptions;

public class UserAlreadySignedException extends Exception {
    public UserAlreadySignedException(String message) {
        super(message);
    }
}
