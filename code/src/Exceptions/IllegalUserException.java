package Exceptions;

public class IllegalUserException extends Exception {
    public IllegalUserException(String message) {
        super(message);
    }
}
