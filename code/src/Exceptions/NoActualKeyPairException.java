package Exceptions;

public class NoActualKeyPairException extends Exception {
    public NoActualKeyPairException(String message) {
        super(message);
    }
}
