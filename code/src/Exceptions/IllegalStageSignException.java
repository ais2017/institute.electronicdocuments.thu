package Exceptions;

public class IllegalStageSignException extends Exception{
    public IllegalStageSignException(String message) {
        super(message);
    }
}
