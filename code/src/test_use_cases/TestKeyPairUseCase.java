package test_use_cases;

import org.json.JSONArray;
import org.junit.Assert;
import org.junit.Test;
import test_int.KeysTestInt;
import test_int.UserTestInt;
import use_cases.AddKeyPairUsecase;

import java.util.UUID;

public class TestKeyPairUseCase {

    @Test
    public void testKeyPairUsecase() {
        UserTestInt ui = new UserTestInt();
        KeysTestInt ki = new KeysTestInt();
        AddKeyPairUsecase keyPairUsecase = new AddKeyPairUsecase(ui, ki);
        try {
            JSONArray usersWithoutKeys = keyPairUsecase.getUsersWithoutActualKeyPair();
            UUID uuid = UUID.fromString(usersWithoutKeys.getJSONObject(0).getString("uuid"));
            keyPairUsecase.generateKeyPairForUser(uuid);
            Assert.assertEquals(ki.getLastKeyPair().getId(), ui.getById(uuid).getActualKeyPair().getId());

            JSONArray usersWithoutKeysByType = keyPairUsecase.getUsersByTypeWithoutActualKeyPair(3);
            //for(int i = 0; i < usersWithoutKeysByType.length(); ++i){
            uuid = UUID.fromString(usersWithoutKeysByType.getJSONObject(0).getString("uuid"));
            keyPairUsecase.generateKeyPairForUser(uuid);
            Assert.assertEquals(uuid, ki.getLastKeyPair().getOwner().getId());
            Assert.assertEquals(ui.getById(uuid).getActualKeyPair().getId(), ki.getLastKeyPair().getId());
            Assert.assertEquals(1, ui.getById(uuid).getKeys().size());
            keyPairUsecase.generateKeyPairForUser(uuid); //генерируем второй ключ для пользователя с типом 3
            Assert.assertEquals(uuid, ki.getLastKeyPair().getOwner().getId());
            Assert.assertEquals(ui.getById(uuid).getActualKeyPair().getId(), ki.getLastKeyPair().getId());
            Assert.assertEquals(2, ui.getById(uuid).getKeys().size());
            //}
            JSONArray usersByType = keyPairUsecase.getUsersByType(2);
            //for(int i = 0; i < usersByType.length(); ++i) {
                uuid = UUID.fromString(usersByType.getJSONObject(0).getString("uuid"));
                keyPairUsecase.generateKeyPairForUser(uuid);
                Assert.assertEquals(uuid, ki.getLastKeyPair().getOwner().getId());
                Assert.assertEquals(ui.getById(uuid).getActualKeyPair().getId(), ki.getLastKeyPair().getId());
            //}*/
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("Failed");
        }
    }
}
