package test_use_cases;

import model.Document;
import model.Status;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import test_int.*;
import model.User;
import use_cases.DocumentUsecase;

import java.util.Map;
import java.util.UUID;

public class TestDocUseCase {

    @Test

    public void testDocUsecase(){
        UserTestInt userTestInt = new UserTestInt();
        User user1 = userTestInt.getUsersWithoutActualKeyPair().get(0);
        User user2 = userTestInt.getUsersWithoutActualKeyPair().get(1);
        User user3 = userTestInt.getUsersWithoutActualKeyPair().get(2);
        User user4 = userTestInt.getUsersWithoutActualKeyPair().get(3);
        try {
            userTestInt.getById(user1.getId()).generateKeyPair(UUID.randomUUID());
            userTestInt.getById(user2.getId()).generateKeyPair(UUID.randomUUID());
            userTestInt.getById(user3.getId()).generateKeyPair(UUID.randomUUID());
            userTestInt.getById(user4.getId()).generateKeyPair(UUID.randomUUID());
        } catch (Exception e) {
            Assert.fail("211"+e.getMessage());
        }
        DocSignStageTestInt docSignStageTestInt = new DocSignStageTestInt();
        DocSignTestInt docSignTestInt = new DocSignTestInt();
        NotificationTestInt notificationTestInt = new NotificationTestInt();
        StorageTestInt storageTestInt = new StorageTestInt();
        DocumentTestInt documentTestInt = new DocumentTestInt(user1, user3.getId(), user4.getId());

        DocumentUsecase documentUsecase = new DocumentUsecase(documentTestInt, notificationTestInt, userTestInt, storageTestInt, docSignTestInt, docSignStageTestInt);
        Map<UUID,Document> pair = documentTestInt.getNotSignedDocumentsForUser(user1.getId());
        Object[] keys = pair.keySet().toArray();
        Document doc = pair.get(keys[0]);
        UUID docId = (UUID)keys[0];
        try {
            Assert.assertEquals(doc.getStatus(), Status.PREPARING.getValue());
            documentUsecase.sendDocumentToSign(user1.getId(), docId);
            Assert.assertEquals(null, notificationTestInt.getNotificationForSigner(user3.getId())); // т.к. это не первая стадия
            Assert.assertEquals(null, notificationTestInt.getNotificationForSigner(user4.getId())); // т.к. это не первая стадия
            Assert.assertEquals(docId, notificationTestInt.getNotificationForSigner(user1.getMaster().getId()).get(0).getValue()); //уведомление для начальника user1
            Assert.assertEquals(null,documentUsecase.getNotificationsForSigner(user3.getId()));
            JSONObject object = documentUsecase.getNotificationsForSigner(user1.getMaster().getId());
            Assert.assertEquals(user1.getId().toString(), object.getJSONArray("documents").getJSONObject(0).getString("creatorId"));
            Assert.assertEquals(doc.getName(), object.getJSONArray("documents").getJSONObject(0).getString("dcoumentName"));
            Assert.assertEquals(Status.PROCESSING.getValue(), documentTestInt.getDocumentById(user1.getId(), docId).getStatus());
            documentUsecase.makeSign(user1.getMaster().getId(), user1.getId(), docId, "Signed successfully!");
            Assert.assertEquals(Status.PROCESSING.getValue(), documentTestInt.getDocumentById(user1.getId(), docId).getStatus());
            Assert.assertEquals(1, documentTestInt.getDocumentById(user1.getId(), docId).getCurrentStageIndex());
            Assert.assertEquals(Status.SIGNED.getValue(), documentTestInt.getDocumentById(user1.getId(), docId).getPreviousStage().getStatus());

            Assert.assertEquals(docId, notificationTestInt.getNotificationForSigner(user3.getId()).get(0).getValue()); //уведомление на второй стадии
            Assert.assertEquals(docId, notificationTestInt.getNotificationForSigner(user4.getId()).get(0).getValue()); //уведомление на второй стадии
            object = documentUsecase.getNotificationsForSigner(user3.getId());
            Assert.assertEquals(user1.getId().toString(), object.getJSONArray("documents").getJSONObject(0).getString("creatorId"));
            Assert.assertEquals(doc.getName(), object.getJSONArray("documents").getJSONObject(0).getString("dcoumentName"));
            Assert.assertEquals(Status.PROCESSING.getValue(), documentTestInt.getDocumentById(user1.getId(), docId).getStatus());
            documentUsecase.refuseSign(user4.getId(), user1.getId(), docId, "Refused because...", "Just refused!");
            Assert.assertEquals(docId, notificationTestInt.getNotificationForDocCreator(user1.getId())[0]); //уведомление создателю о готовности документа
            Assert.assertEquals(Status.REFUSED.getValue(), documentTestInt.getDocumentById(user1.getId(), docId).getStatus());
            Assert.assertEquals("Just refused!", documentTestInt.getDocumentById(user1.getId(), docId).getCommentOfLastSign());
            object = documentUsecase.getNotificationsForCreator(user1.getId());
            Assert.assertEquals(docId.toString(), object.getJSONArray("notifications").getJSONObject(0).getString("uuid"));
            Assert.assertEquals(doc.getName(), object.getJSONArray("notifications").getJSONObject(0).getString("name"));
            Assert.assertEquals(Status.REFUSED.getValue(), object.getJSONArray("notifications").getJSONObject(0).getString("status"));
            Assert.assertEquals("Just refused!", object.getJSONArray("notifications").getJSONObject(0).getString("comment"));
        } catch (Exception e) {
            //System.out.println(e.getMessage());
            Assert.fail(e.getMessage());
        }
    }
}
