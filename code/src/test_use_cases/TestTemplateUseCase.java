package test_use_cases;

import javafx.util.Pair;
import model.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import test_int.DocTemplateTestInt;
import test_int.StageDescrTestInt;
import test_int.UserTestInt;
import use_cases.TemplateUsecase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class TestTemplateUseCase {

    @Test

    public void testDocTemplateUsecase() {
        UserTestInt ui = new UserTestInt();
        User user3 = ui.getUsersWithoutActualKeyPair().get(2);
        User user4 = ui.getUsersWithoutActualKeyPair().get(3);
        DocTemplateTestInt di = new DocTemplateTestInt(user3.getId(), user4.getId());
        StageDescrTestInt si = new StageDescrTestInt();
        TemplateUsecase templateUsecase = new TemplateUsecase(di, si, ui);
        try {
            JSONArray array = templateUsecase.getActualTemplatesForUser(0);
            JSONObject t = array.getJSONObject(0);
            //for(int i = 0; i < array.length(); ++i) {
                //DocTemplate doc = templateUsecase.jsonToTemplate(array.getJSONObject(i));
            byte [] tt = t.get("template").toString().getBytes();
            int userType = t.getInt("userType");
            String name = t.getString("name");
            boolean isActual = true;
            Calendar date = Calendar.getInstance();
            List<StageDescription> descriptions = new ArrayList<>();
            JSONArray array1 = t.getJSONArray("descriptions");
            if(array.length() == 0)
                throw new Exception("Empty signer list!");
            for (int i = array1.length()-1; i >= 0; --i) {
                SignerRole role;
                WayOfStagePassing wayOfStagePassing;
                StageDescription stageDescription;
                UUID descrId = UUID.randomUUID();
                StageDescription nextdescr;
                if (descriptions.isEmpty())
                    nextdescr = null;
                else
                    nextdescr = descriptions.get(0);
                if (array1.getJSONObject(i).getString("wayOfPassing").equals(WayOfStagePassing.ALL.getValue()))
                    wayOfStagePassing = WayOfStagePassing.ALL;
                else if (array1.getJSONObject(i).getString("wayOfPassing").equals(WayOfStagePassing.ANY.getValue()))
                    wayOfStagePassing = WayOfStagePassing.ANY;
                else
                    throw new Exception("Error way of stage passing!");
                if (array1.getJSONObject(i).getString("signerRole").equals(SignerRole.ABSOLUTE.getValue())) {
                    role = SignerRole.ABSOLUTE;
                    JSONArray signers = array1.getJSONObject(i).getJSONArray("signers");
                    if (signers == null || signers.length() == 0)
                        throw new Exception("Empty list of signers!");
                    UUID[] signersId = new UUID[signers.length()];
                    for (int j = 0; j < signers.length(); ++j)
                        signersId[j] = UUID.fromString(signers.getJSONObject(j).getString("uuid"));
                    try {
                        stageDescription = new StageDescription(nextdescr, wayOfStagePassing, signersId, role, descrId);
                    } catch (Exception e) {
                        throw new Exception("Error when creating stage description: " + e.getMessage());
                    }
                } else if (array1.getJSONObject(i).getString("signerRole").equals(SignerRole.RELATIVE.getValue())) {
                    role = SignerRole.RELATIVE;
                    try {
                        stageDescription = new StageDescription(nextdescr, wayOfStagePassing, role, descrId);
                    } catch (Exception e) {
                        throw new Exception("Error when creating stage description: " + e.getMessage());
                    }
                } else
                    throw new Exception("Error signer role!");
                descriptions.add(0, stageDescription);
            }
                DocTemplate doc = new DocTemplate(tt, userType, name, descriptions, date, isActual);
                Assert.assertEquals(0,doc.getUserType());
                Assert.assertEquals(2, doc.getDescriptions().size());
                Assert.assertEquals(WayOfStagePassing.ALL.getValue(), doc.getDescriptions().get(0).getWayPass());
                Assert.assertEquals(WayOfStagePassing.ANY.getValue(), doc.getDescriptions().get(1).getWayPass());
                Assert.assertEquals(SignerRole.RELATIVE.getValue(), doc.getDescriptions().get(0).getRole());
                Assert.assertEquals(SignerRole.ABSOLUTE.getValue(), doc.getDescriptions().get(1).getRole());
                Assert.assertEquals("Template 1", doc.getName());
                UUID docId = UUID.fromString(array.getJSONObject(0).getString("uuid"));
                templateUsecase.setNotActualTemplate(docId);
                Assert.assertEquals(null, templateUsecase.getActualTemplatesForUser(0));
                Assert.assertEquals(1, templateUsecase.getTemplatesForUserType(0).length());
            JSONObject object = templateUsecase.getTemplateByName("Template 1");
            Assert.assertEquals(docId, UUID.fromString(object.getString("uuid")));
            Assert.assertEquals(false, object.getBoolean("isActual"));
            Assert.assertEquals(1, templateUsecase.getAllTemplates().length());

            JSONObject userGroups = templateUsecase.getUserGroups();
            JSONObject signerList = templateUsecase.getSigners(userGroups.getJSONArray("userGroups").getInt(2));

            JSONObject template = new JSONObject();
            template.put("template", tt);
            template.put("userType", userGroups.getJSONArray("userGroups").getInt(0));
            template.put("name", "New template");
            JSONObject stage0 = new JSONObject();
            stage0.put("wayOfPassing", WayOfStagePassing.ALL.getValue());
            stage0.put("signerRole", SignerRole.RELATIVE.getValue());
            stage0.put("signers", JSONObject.NULL);
            JSONObject stage1 = new JSONObject();
            stage1.put("wayOfPassing", WayOfStagePassing.ALL.getValue());
            stage1.put("signerRole", SignerRole.ABSOLUTE.getValue());
            stage1.put("signers", signerList.getJSONArray("signers"));
            JSONArray descriptionsJson = new JSONArray();
            descriptionsJson.put(stage0);
            descriptionsJson.put(stage1);
            template.put("descriptions", descriptionsJson);
            templateUsecase.createTemplate(template);

            Pair<UUID, DocTemplate> pair1 = di.getByName("New template");
            Assert.assertEquals("New template", pair1.getValue().getName());
            Assert.assertEquals(2, pair1.getValue().getDescriptions().size());
            Assert.assertEquals(SignerRole.ABSOLUTE.getValue(), pair1.getValue().getDescriptions().get(0).getNextStageDescription().getRole());
            Assert.assertEquals(WayOfStagePassing.ALL.getValue(), pair1.getValue().getDescriptions().get(0).getNextStageDescription().getWayPass());
            Assert.assertEquals(null, pair1.getValue().getDescriptions().get(1).getNextStageDescription());
            Assert.assertEquals(SignerRole.RELATIVE.getValue(), pair1.getValue().getDescriptions().get(0).getRole());
            Assert.assertEquals(true, pair1.getValue().getDescriptions().get(1).checkIfLast());
            Assert.assertEquals(true, pair1.getValue().checkIfActual());
            //}

        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }
}
