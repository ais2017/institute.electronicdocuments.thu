package use_cases;

import interfaces.DocTemplateInterface;
import interfaces.StageDescrInterface;
import interfaces.UserInterface;
import javafx.util.Pair;
import model.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class TemplateUsecase {

    private DocTemplateInterface docTemplateTestInt;
    private StageDescrInterface stageDescrTestInt;
    private UserInterface userTestInt;

    public TemplateUsecase(DocTemplateInterface di, StageDescrInterface si, UserInterface ui) {
        docTemplateTestInt = di;
        stageDescrTestInt = si;
        userTestInt = ui;
    }

    public void createTemplate(JSONObject template) throws Exception{
        try {
            DocTemplate t = jsonToTemplate(template);
            docTemplateTestInt.addDocTemplate(t);
        } catch (Exception e) {
            throw new Exception("Error when saving doc template: "+e.getMessage());
        }
    }


    public JSONArray getAllTemplates() throws Exception {
        Map<UUID, DocTemplate> templates = docTemplateTestInt.getAllTemplates();
        if(templates.isEmpty())
            return null;
        JSONArray allTemplates = new JSONArray();
        for(Map.Entry<UUID, DocTemplate> entry : templates.entrySet()) {
            try {
                JSONObject template = templateToJson(new Pair<UUID, DocTemplate>(entry.getKey(), entry.getValue()));
                allTemplates.put(template);
            } catch (Exception e) {
                throw new Exception("Error while creating json object: "+e.getMessage());
            }
        }
        return allTemplates;
    }

    public JSONArray getActualTemplatesForUser(int userType) throws Exception {
        Map<UUID, DocTemplate> templates = docTemplateTestInt.getActualTemplatesForUser(userType);
        if(templates.isEmpty())
            return null;
        JSONArray allTemplates = new JSONArray();
        for(Map.Entry<UUID, DocTemplate> entry : templates.entrySet()) {
            try {
                JSONObject template = templateToJson(new Pair<UUID, DocTemplate>(entry.getKey(), entry.getValue()));
                allTemplates.put(template);
            } catch (Exception e) {
                throw new Exception("Error while creating json object: "+e.getMessage());
            }
        }
        return allTemplates;
    }

    public JSONArray getTemplatesForUserType(int userType) throws Exception{
        Map<UUID, DocTemplate> templates = docTemplateTestInt.getTemplatesForUser(userType);
        if(templates.isEmpty())
            return null;
        JSONArray allTemplates = new JSONArray();
        for(Map.Entry<UUID, DocTemplate> entry : templates.entrySet()) {
            try {
                JSONObject template = templateToJson(new Pair<UUID, DocTemplate>(entry.getKey(), entry.getValue()));
                allTemplates.put(template);
            } catch (Exception e) {
                throw new Exception("Error while creating json object: "+e.getMessage());
            }
        }
        return allTemplates;
    }

    public void setNotActualTemplate(UUID tId) {
        DocTemplate t = docTemplateTestInt.getById(tId);
        t.setNotActual();
        docTemplateTestInt.updateTemplate(t, tId);
    }

    private DocTemplate jsonToTemplate(JSONObject t) throws Exception{
        if(t == null)
            throw new IllegalArgumentException("Empty template!");
        byte [] tt = t.get("template").toString().getBytes();
        int userType = t.getInt("userType");
        String name = t.getString("name");
        boolean isActual = true;
        Calendar date = Calendar.getInstance();
        List<StageDescription> descriptions = new ArrayList<>();
        JSONArray array = t.getJSONArray("descriptions");
        if(array.length() == 0)
            throw new Exception("Empty signer list!");
        for (int i = array.length()-1; i >= 0; --i) {
            SignerRole role;
            WayOfStagePassing wayOfStagePassing;
            StageDescription stageDescription;
            UUID descrId = UUID.randomUUID();
            StageDescription nextdescr;
            if(descriptions.isEmpty())
                nextdescr = null;
            else
                nextdescr = descriptions.get(0);
            if(array.getJSONObject(i).getString("wayOfPassing").equals(WayOfStagePassing.ALL.getValue()))
                wayOfStagePassing = WayOfStagePassing.ALL;
            else if(array.getJSONObject(i).getString("wayOfPassing").equals(WayOfStagePassing.ANY.getValue()))
                wayOfStagePassing = WayOfStagePassing.ANY;
            else
                throw new Exception("Error way of stage passing!");
            if(array.getJSONObject(i).getString("signerRole").equals(SignerRole.ABSOLUTE.getValue())) {
                role = SignerRole.ABSOLUTE;
                JSONArray signers = array.getJSONObject(i).getJSONArray("signers");
                if(signers == null || signers.length() == 0)
                    throw new Exception("Empty list of signers!");
                UUID [] signersId = new UUID[signers.length()];
                for (int j = 0; j < signers.length(); ++j)
                    signersId[j] = UUID.fromString(signers.getJSONObject(j).getString("uuid"));
                try {
                    stageDescription = new StageDescription(nextdescr, wayOfStagePassing, signersId, role, descrId);
                } catch (Exception e) {
                    throw new Exception("Error when creating stage description: "+e.getMessage());
                }
            }
            else if(array.getJSONObject(i).getString("signerRole").equals(SignerRole.RELATIVE.getValue())) {
                role = SignerRole.RELATIVE;
                try {
                    stageDescription = new StageDescription(nextdescr, wayOfStagePassing, role, descrId);
                } catch (Exception e) {
                    throw new Exception("Error when creating stage description: "+e.getMessage());
                }
            }
            else
                throw new Exception("Error signer role!");
            descriptions.add(0, stageDescription);
        }
        try {
            DocTemplate docTemplate = new DocTemplate(tt, userType, name, descriptions, date, isActual);
            return docTemplate;
        } catch (Exception e) {
            throw new Exception("Error when creating docTemplate: "+e.getMessage());
        }
    }

    private JSONObject templateToJson(Pair<UUID, DocTemplate> p) throws Exception{
        JSONObject t = new JSONObject();
        JSONArray descriptions = new JSONArray();
            t.put("uuid", p.getKey());
            t.put("userType", p.getValue().getUserType());
            t.put("template", p.getValue().getTemplate());
            t.put("name", p.getValue().getName());
            t.put("date", p.getValue().getCreationDate());
            t.put("isActual", p.getValue().checkIfActual());
            for (int i = 0; i < p.getValue().getDescriptions().size(); ++i) {
                JSONObject descr = new JSONObject();
                descr.put("uuid", p.getValue().getDescriptions().get(i).getId());
                descr.put("wayOfPassing", p.getValue().getDescriptions().get(i).getWayPass());
                descr.put("signerRole", p.getValue().getDescriptions().get(i).getRole());
                if (p.getValue().getDescriptions().get(i).getNextStageDescription() != null)
                    descr.put("nextDescrId", p.getValue().getDescriptions().get(i).getNextStageDescription().getId());
                else
                    descr.put("nextDescrId", JSONObject.NULL);
                JSONArray signers = new JSONArray();
                if (p.getValue().getDescriptions().get(i).getRole().equals(SignerRole.RELATIVE.getValue())) {
                    JSONObject signer = new JSONObject();
                    signer.put("uuid", JSONObject.NULL);
                } else {
                    for (int j = 0; j < p.getValue().getDescriptions().get(i).getSigners().length; ++j) {
                        JSONObject signer = new JSONObject();
                        signer.put("uuid", p.getValue().getDescriptions().get(i).getSigners()[j]);
                        signers.put(signer);
                    }
                }
                descr.put("signers", signers);
                descriptions.put(descr);
            }
            t.put("descriptions", descriptions);
            return t;
    }

    public JSONObject getTemplateByName(String name) throws Exception {
        try {
            Pair<UUID, DocTemplate> p = docTemplateTestInt.getByName(name);
            try {
                return templateToJson(p);
            } catch (Exception e) {
                throw new Exception("Error while creating json object: " + e.getMessage());
            }
        } catch (Exception e) {
            throw new Exception("Error while getting template by name: "+e.getMessage());
        }
    }

    public JSONObject getSigners(int userType) throws Exception{
        List<User> signers = userTestInt.getSignersByType(userType);
        if(signers == null || signers.isEmpty())
            return null;
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        try {
            for (int i = 0; i < signers.size(); ++i) {
                JSONObject pair = new JSONObject();
                pair.put("uuid", signers.get(i).getId());
                pair.put("name", signers.get(i).getName());
                pair.put("userType", signers.get(i).getUserType());
                array.put(pair);
            }
            object.put("signers", array);
            return object;
        } catch (Exception e) {
            throw new Exception("Error when creating json object: "+e.getMessage());
        }
    }

    public JSONObject getUserGroups() throws Exception{
        int[] signers = userTestInt.getUserTypes();
        if(signers == null || signers.length == 0)
            return null;
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        for(int i = 0; i < signers.length; ++i)
            array.put(signers[i]);
        try {
            object.put("userGroups", array);
        } catch (Exception e) {
            throw new Exception("Error when creating json object: "+e.getMessage());
        }
        return object;
    }
}
