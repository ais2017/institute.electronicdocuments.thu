package use_cases;

import interfaces.*;
import javafx.util.Pair;
import model.*;
import org.json.JSONArray;
import org.json.JSONObject;
import test_int.*;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DocumentUsecase {
    private DocumentInterface docTestInt;
    private NotificationInterface notificationTestInt;
    private UserInterface userTestInt;
    private StorageInterface storageTestInt;
    private DocSignInterface docSignTestInt;
    private DocSignStageInterface docSignStageTestInt;

    public DocumentUsecase(DocumentInterface di, NotificationInterface ni, UserInterface ui, StorageInterface si, DocSignInterface dsi, DocSignStageInterface dssi) {
        docTestInt = di;
        notificationTestInt = ni;
        userTestInt = ui;
        storageTestInt= si;
        docSignTestInt = dsi;
        docSignStageTestInt = dssi;
    }

    public JSONObject getNotificationsForSigner(UUID uId) throws Exception {
        List<Pair<UUID, UUID>> pairs = notificationTestInt.getNotificationForSigner(uId);
        if(pairs == null || pairs.isEmpty())
            return null;
        JSONObject documents = new JSONObject();
        JSONArray array = new JSONArray();
        for(int i = 0; i < pairs.size(); ++i) {
            Document doc = docTestInt.getDocumentById(pairs.get(i).getKey(), pairs.get(i).getValue());
            if(doc == null)
                throw new Exception("Error! Document with userId "+pairs.get(i).getKey()+" and docId "+pairs.get(i).getValue()+" not found!");
            JSONObject document = new JSONObject();
            try {
                document.put("creatorId", doc.getCreator().getId());
                document.put("userType", doc.getCreator().getUserType());
                document.put("userName", doc.getCreator().getName());
                document.put("docId", pairs.get(i).getValue());
                document.put("document", doc.getDoc());
                document.put("dcoumentName", doc.getName());
                } catch (Exception e) {
                throw new Exception("Error when creating json object: "+e.getMessage());
            }
            array.put(document);
        }
        try {
            documents.put("documents", array);
            return documents;
        } catch (Exception e) {
            throw new Exception("Error when saving json array!");
        }
    }

    public JSONObject getNotificationsForCreator(UUID uId) throws Exception {
        UUID[] docIds = notificationTestInt.getNotificationForDocCreator(uId);
        if(docIds == null || docIds.length == 0)
            return null;
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        for (int i = 0; i < docIds.length; ++i) {
            Document doc = docTestInt.getDocumentById(uId, docIds[i]);
            JSONObject obj = new JSONObject();
            try {
                obj.put("uuid", docIds[i]);
                obj.put("name", doc.getName());
                obj.put("status", doc.getStatus());
                if(doc.getCommentOfLastSign() != null)
                    obj.put("comment", doc.getCommentOfLastSign());
                else
                    obj.put("comment", JSONObject.NULL);
                array.put(obj);
            } catch (Exception e) {
                throw new Exception("Error when creating json object!");
            }
        }
        try {
            object.put("notifications", array);
            return object;
        } catch (Exception e) {
            throw new Exception("Error when saving json array: "+e.getMessage());
        }
    }

    public void sendDocumentToSign(UUID uId, UUID docId) throws Exception {
        Document doc = docTestInt.getDocumentById(uId, docId);
        if(doc == null)
            throw new IllegalArgumentException("Illegal ids!");
        if(!doc.setEnableToSign()) {
            if(doc.getStatus().equals(Status.REFUSED.getValue()))
                throw new IllegalArgumentException("The document is refused!");
            if(doc.getStatus().equals(Status.SIGNED.getValue()))
                throw new IllegalArgumentException("The document is signed!");
            if(doc.getStatus().equals(Status.PROCESSING.getValue()))
                throw new IllegalArgumentException("The document already was sent to signers!");
        }
        docTestInt.updateDocument(docId, uId, doc);
        if(doc.getCurrentStage().getDescription().getRole().equals(SignerRole.ABSOLUTE.getValue())) {
            notificationTestInt.sendNotificationToSigners(uId, docId, doc.getCurrentStage().getDescription().getSigners());
        }
        else {
            if(doc.getCreator().getMaster() == null)
                System.out.println("0000");
            UUID[] signer = {doc.getCreator().getMaster().getId()};
            notificationTestInt.sendNotificationToSigners(uId, docId, signer);
        }
    }

    public void makeSign(UUID signerId, UUID creatorId, UUID docId, String status) throws Exception{
        Document doc = docTestInt.getDocumentById(creatorId, docId);
        User signer = userTestInt.getById(signerId);
        if(doc == null || signer == null)
            throw new IllegalArgumentException("Illegal ids!");
        try {
            UUID signId = UUID.randomUUID();
            DocSign sign = doc.signDocument(status, signer, signId);
            docSignTestInt.addDocSign(creatorId, docId, signerId, sign);
            if(doc.getCurrentStage().getSigns().isEmpty()) { // значит, что перешли на новый этап
                if(doc.getPreviousStage() != null)
                    docSignStageTestInt.updateSignStage(creatorId, docId, doc.getPreviousStage());
                notificationTestInt.sendNotificationToSigners(creatorId, docId, doc.getSignersIdOfCurrentStage());
            }
            else {
                if(doc.getStatus().equals(Status.SIGNED.getValue())) {
                    docTestInt.updateDocument(docId, creatorId, doc);
                    storageTestInt.saveToStorage(doc);
                    notificationTestInt.sendNotificationToCreatorAboutDocStatusChange(creatorId, docId);
                }
            }
        } catch (Exception e) {
            throw new Exception("Error while signing a document: "+e.getMessage());
        }
    }

    public void refuseSign(UUID signerId, UUID creatorId, UUID docId, String status, String comment) throws Exception{
        Document doc = docTestInt.getDocumentById(creatorId, docId);
        User signer = userTestInt.getById(signerId);
        if(doc == null || signer == null)
            throw new IllegalArgumentException("Illegal ids!");
        try {
            UUID signId = UUID.randomUUID();
            DocSign sign = doc.refuseToSign(status, comment, signer, signId);
            docSignTestInt.addDocSign(creatorId, docId, signerId, sign);
            docSignStageTestInt.updateSignStage(creatorId, docId, doc.getCurrentStage());
            docTestInt.updateDocument(docId, creatorId, doc);
            storageTestInt.saveToStorage(doc);
            notificationTestInt.sendNotificationToCreatorAboutDocStatusChange(creatorId, docId);
        } catch (Exception e) {
            throw new Exception("Error while refusing to sign a document: "+e.getMessage());
        }
    }
}
