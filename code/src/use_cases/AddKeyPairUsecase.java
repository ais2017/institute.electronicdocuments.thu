package use_cases;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import interfaces.KeyInterface;
import interfaces.UserInterface;
import model.KeysPair;
import model.User;
import netscape.javascript.JSObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import test_int.KeysTestInt;
import test_int.UserTestInt;

public class AddKeyPairUsecase {

    private UserInterface userTestInt;
    private KeyInterface keysTestInt;

    public AddKeyPairUsecase(UserInterface ui, KeyInterface ki){
        userTestInt = ui;
        keysTestInt = ki;
    }

    public JSONArray getUsersByType(int userType) throws Exception{
        List<User> users = userTestInt.getUsersByType(userType);
        if(users == null)
            return null;
        JSONArray usersInJson = new JSONArray();
        for(int i = 0; i < users.size(); ++i) {
            try {
                JSONObject user = new JSONObject();
                user.put("name", users.get(i).getName());
                user.put("uuid", users.get(i).getId());
                user.put("userType", users.get(i).getUserType());
                usersInJson.put(user);
            } catch (Exception e) {
                throw new Exception("Error while creating jsonArray: "+e.getMessage());
            }
        }
        return usersInJson;
    }

    public JSONArray getUsersByTypeWithoutActualKeyPair(int userType) throws Exception{
        List<User> users = userTestInt.getUsersByType(userType);
        if(users == null)
            return null;
        JSONArray usersInJson = new JSONArray();
        for(int i = 0; i < users.size(); ++i) {
            if(users.get(i).getActualKeyPair() == null) {
                try {
                    JSONObject user = new JSONObject();
                    user.put("name", users.get(i).getName());
                    user.put("uuid", users.get(i).getId());
                    user.put("userType", users.get(i).getUserType());
                    usersInJson.put(user);
                } catch (Exception e) {
                    throw new Exception("Error while creating jsonArray: " + e.getMessage());
                }
            }
        }
        return usersInJson;
    }

    public JSONArray getUsersWithoutActualKeyPair() throws Exception {
        List<User> users = userTestInt.getUsersWithoutActualKeyPair();
        if(users == null || users.isEmpty())
            return null;
        JSONArray usersInJson = new JSONArray();
        for(int i = 0; i < users.size(); ++i) {
            try {
                JSONObject user = new JSONObject();
                user.put("name", users.get(i).getName());
                user.put("uuid", users.get(i).getId());
                user.put("userType", users.get(i).getUserType());
                usersInJson.put(user);
            } catch (Exception e) {
                throw new Exception("Error while creating jsonArray: "+e.getMessage());
            }
        }
        return usersInJson;
    }

    public void generateKeyPairForUser(UUID id) throws Exception {
        User user = userTestInt.getById(id);
        if(user == null)
            throw new IllegalArgumentException("User with id "+id+" not found!");
        if(user.getActualKeyPair() != null) {
            KeysPair oldKey = user.getActualKeyPair();
            oldKey.setNotActual();
            keysTestInt.updateKeyPair(oldKey);
        }
        try {
            user.generateKeyPair(UUID.randomUUID());
            keysTestInt.addKeyPair(user.getActualKeyPair());
        } catch (Exception e) {
            throw new Exception("Error while generating keys for user id = "+id+" :"+e.getMessage());
        }
    }

    private KeysPair getKeys(){
        return null;
    };
}
