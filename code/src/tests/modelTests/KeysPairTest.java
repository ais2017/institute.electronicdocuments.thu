package tests.modelTests;

import model.KeysPair;
import model.User;
import org.junit.Assert;
import org.junit.Test;

import java.security.KeyPair;
import java.util.Calendar;
import java.util.UUID;

public class KeysPairTest {
    @Test

    public void testKeys() {
        UUID id = UUID.randomUUID();
        User user = new User(0, null, id, null, "Dan");
        UUID kId = UUID.randomUUID();
        KeysPair k1;
        try {
            user.generateKeyPair(kId);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertEquals(kId, user.getActualKeyPair().getId());
        Assert.assertEquals(true, user.getActualKeyPair().getActualFlag());
        Assert.assertEquals(id, user.getActualKeyPair().getOwner().getId());
        try {
             k1 = new KeysPair(null, null, null, null, null, false); //empty owner
            Assert.fail("Constructor failed!");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        try {
             k1 = new KeysPair(user, null, null, null, null, false); // empty uuid
            Assert.fail("Constructor failed!");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        try {
             k1 = new KeysPair(user, kId, null, null, null, false); // empty key
            Assert.fail("Constructor failed!");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        try { // empty date
             k1 = new KeysPair(user, kId, null, null, new KeyPair(user.getActualKeyPair().getPublicKey(), user.getActualKeyPair().getPrivateKey()), false);
            Assert.fail("Constructor failed!");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        try {
             k1 = new KeysPair(user, kId, user.getActualKeyPair().getStartDate(), user.getActualKeyPair().getEndDate(), new KeyPair(user.getActualKeyPair().getPublicKey(), user.getActualKeyPair().getPrivateKey()), true);
             Assert.assertEquals(user, k1.getOwner());
             Assert.assertEquals(kId, k1.getId());
             Assert.assertEquals(user.getActualKeyPair().getStartDate(), k1.getStartDate());
             Assert.assertEquals(user.getActualKeyPair().getEndDate(), k1.getEndDate());
             Assert.assertEquals(user.getActualKeyPair().getPrivateKey(), k1.getPrivateKey());
             Assert.assertEquals(user.getActualKeyPair().getPublicKey(), k1.getPublicKey());
             Assert.assertEquals(true, k1.getActualFlag());
             Assert.assertEquals(true, k1.checkIfActual(user.getActualKeyPair().getStartDate()));
             Calendar d = Calendar.getInstance(user.getActualKeyPair().getStartDate().getTimeZone());
             d.add(Calendar.YEAR, 2);
             Assert.assertEquals(true, k1.checkIfActual(d));
             k1.setNotActual();
             Assert.assertEquals(false, k1.getActualFlag());
             Assert.assertEquals(false, k1.checkIfActual(d));
             Assert.assertEquals(true, k1.checkKeyDates(d));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            Assert.fail("Constructor failed!");
        }
    }
}
