package tests.modelTests;

import model.KeysPair;
import model.User;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class UserTest {

    @Test
    public void testUser() {
        UUID id = UUID.randomUUID();
        UUID kId2 = UUID.randomUUID();
        UUID id1 = UUID.randomUUID();
        List<KeysPair> keys = new ArrayList<KeysPair>();
        try {
            User u3 = new User(1,null, null, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) { //пустой uuid
            System.out.println(e.getMessage());
        }

        try {
            User u2 = new User(1, null, id1, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) { //пустой username
            System.out.println(e.getMessage());
        }
        User u2 = new User(1, null, id1, null, "Ann"); //все ок
        try {
            keys.add(new KeysPair(u2, kId2));
            keys.add(new KeysPair(u2, id));
            Assert.assertEquals(true, keys.get(0).getActualFlag());
            Assert.assertEquals(id1, keys.get(0).getOwner().getId());
            Assert.assertEquals(true, keys.get(1).getActualFlag());
            Assert.assertEquals(id1, keys.get(1).getOwner().getId());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("Key generation failed!");
        }
        try {
            User u = new User(0, null, id, keys, "Ann"); //несоответствие userId
            Assert.fail("User container failed");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            u2.setKeys(keys); //2 актуальных ключа
            Assert.fail("User set keys failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        User u1 = new User(0, u2, id, null, "Ann");
        Assert.assertEquals(0, u1.getUserType());
        Assert.assertEquals(u2, u1.getMaster());
        Assert.assertEquals(id, u1.getId());
        Assert.assertEquals(null, u1.getActualKeyPair());
        UUID kId0 = UUID.randomUUID();
        UUID kId1 = UUID.randomUUID();
        try {
            u1.generateKeyPair(kId0);
            Assert.assertEquals(kId0, u1.getActualKeyPair().getId());
            Assert.assertEquals(id, u1.getActualKeyPair().getOwner().getId());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("Generate keys failed!");
        }
        try {
            KeysPair oldK = u1.getActualKeyPair();
            u1.generateKeyPair(kId1); //Было 2 актуальных ключа, установили 1 как неактуальный
            Assert.assertEquals(oldK.getId(), u1.getKeyPairForDate(oldK.getStartDate()).getId());
            Assert.assertEquals(false, u1.getKeyPairForDate(oldK.getStartDate()).getActualFlag());
            Assert.assertEquals(kId1, u1.getActualKeyPair().getId());
            u1.getActualKeyPair().setNotActual();
            Assert.assertEquals(null, u1.getActualKeyPair()); // нет актуальных ключей
            Assert.assertEquals(kId1, u1.getKeyPairForDate(Calendar.getInstance()).getId());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("2 Generate keys failed");
        }
        try {
            u1.generateKeyPair(kId1); // добавили новый ключ
            Assert.assertEquals(kId1, u1.getActualKeyPair().getId());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
            System.out.println(e.getMessage());
        }
    }
}
