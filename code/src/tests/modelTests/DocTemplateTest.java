package tests.modelTests;

import model.DocTemplate;
import model.SignerRole;
import model.StageDescription;
import model.WayOfStagePassing;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class DocTemplateTest {
    @Test

    public void testTemplate() {
        byte[] tt = new byte[] {(byte)0x65, (byte)0x37};
        DocTemplate t;
        Calendar date = Calendar.getInstance();
        /*date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);*/
        try {
            t = new DocTemplate(null, 0, "Template 1", null, date, true); //empty template
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            t = new DocTemplate(tt, 0, null, null, date, true); // empty name
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            t = new DocTemplate(tt, 0, "Template 1", null, date, true); // empty descr
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        UUID [] signersId = {UUID.randomUUID(), UUID.randomUUID()};
        UUID [] signersId1 = {UUID.randomUUID(), UUID.randomUUID()};
        StageDescription last = new StageDescription(null, WayOfStagePassing.ANY, signersId, SignerRole.ABSOLUTE, UUID.randomUUID());
        StageDescription second = new StageDescription(last, WayOfStagePassing.ALL, signersId1, SignerRole.ABSOLUTE, UUID.randomUUID());
        StageDescription first = new StageDescription(last, WayOfStagePassing.ALL, SignerRole.RELATIVE, UUID.randomUUID());
        List<StageDescription> stages = new ArrayList<StageDescription>();
        //stages.add(first);
        stages.add(second);
        try {
            t = new DocTemplate(tt, 0, "Template 1", stages, null, true); // no last stage
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        stages.add(last);
        try { // empty date
            t = new DocTemplate(tt, 0, "Template 1", stages, null, true);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        stages.add(0, first);
        try { // Illegal stages
            t = new DocTemplate(tt, 0, "Template 1", stages, date, true);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        StageDescription first1 = new StageDescription(second, WayOfStagePassing.ALL, SignerRole.RELATIVE, UUID.randomUUID());
        stages.set(0, first1);
        try {
            t = new DocTemplate(tt, 0, "Template 1", stages, date, true);
            Assert.assertEquals(tt, t.getTemplate());
            Assert.assertEquals(0, t.getUserType());
            Assert.assertEquals(stages.size(), t.getDescriptions().size());
            Assert.assertEquals(date, t.getCreationDate());
            Assert.assertEquals("Template 1", t.getName());
            Assert.assertEquals(true, t.checkIfActual());
            t.setNotActual();
            Assert.assertEquals(false, t.checkIfActual());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("Constructor failed1!");
        }
        try {
            t = new DocTemplate(tt, 1, "Template 1", stages);
            Assert.assertEquals(tt, t.getTemplate());
            Assert.assertEquals(1, t.getUserType());
            Assert.assertEquals(stages.size(), t.getDescriptions().size());
            Assert.assertEquals("Template 1", t.getName());
            Assert.assertEquals(true, t.checkIfActual());
            t.setNotActual();
            Assert.assertEquals(false, t.checkIfActual());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("Constructor failed!");
        }
    }
}
