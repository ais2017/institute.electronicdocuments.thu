package tests.modelTests;

import model.*;
import org.junit.Assert;
import org.junit.Test;

import Exceptions.NoActualKeyPairException;
//import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class DocumentTest {
    @Test

    public void testDocumnet() {
        try {
            Document doc = new Document(null, null, null, null, null, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            Document doc = new Document("Doc1", null, null, null, null, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        byte[] tt = new byte[] {(byte)0x65, (byte)0x37};
        try {
            Document doc = new Document("Doc1", tt, null, null, null, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        Calendar startDate = Calendar.getInstance();
        startDate.set(Calendar.HOUR_OF_DAY, 0);
        startDate.set(Calendar.MINUTE, 0);
        startDate.set(Calendar.SECOND, 0);
        startDate.set(Calendar.MILLISECOND, 0);
        try {
            Document doc = new Document("Doc1", tt, startDate, null, null, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            Document doc = new Document("Doc1", tt, startDate, null, new User(0, null, UUID.randomUUID(), null, "Marie"), null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            StageDescription first = new StageDescription(null, WayOfStagePassing.ALL, SignerRole.RELATIVE, UUID.randomUUID());
            List<StageDescription> stages = new ArrayList<StageDescription>();
            stages.add(first);
            UUID[] signersId = {UUID.randomUUID(), UUID.randomUUID()};
            StageDescription descr = new StageDescription(null, WayOfStagePassing.ALL, signersId, SignerRole.ABSOLUTE, UUID.randomUUID());
            DocSignStage stage = new DocSignStage(null, descr, UUID.randomUUID());
            List<DocSignStage> stages1 = new ArrayList<DocSignStage>();
            stages1.add(stage);
            DocTemplate t = new DocTemplate(tt, 1, "Template 1", stages);
            Document doc = new Document("Doc1", tt, startDate, t, new User(0, null, UUID.randomUUID(), null, "Marie"), null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            UUID[] signersId = {UUID.randomUUID(), UUID.randomUUID()};
            UUID id = UUID.randomUUID();
            StageDescription descr = new StageDescription(null, WayOfStagePassing.ALL, signersId, SignerRole.ABSOLUTE, UUID.randomUUID());
            User user3 = new User(0, null, descr.getSigners()[1], null, "Marie");
            User user1 = new User(1, null, UUID.randomUUID(), null, "Marie");
            user3.generateKeyPair(UUID.randomUUID());
            UUID [] signersId1 = {UUID.randomUUID(), UUID.randomUUID()};
            StageDescription last = new StageDescription(null, WayOfStagePassing.ANY, signersId, SignerRole.ABSOLUTE, UUID.randomUUID());
            StageDescription second = new StageDescription(last, WayOfStagePassing.ALL, signersId1, SignerRole.ABSOLUTE, UUID.randomUUID());
            StageDescription first = new StageDescription(second, WayOfStagePassing.ALL, SignerRole.RELATIVE, UUID.randomUUID());
            List<StageDescription> stages = new ArrayList<StageDescription>();
            stages.add(first);
            stages.add(second);
            stages.add(last);
            DocSignStage stage = new DocSignStage(null, descr, id);
            List<DocSignStage> stages1 = new ArrayList<DocSignStage>();
            stages1.add(stage);
            DocTemplate t = new DocTemplate(tt, 1, "Template 1", stages);
            Document doc2 = new Document("Doc2", tt, startDate, t, user1, stages1, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            UUID[] signersId = {UUID.randomUUID(), UUID.randomUUID()};
            UUID id = UUID.randomUUID();
            StageDescription descr = new StageDescription(null, WayOfStagePassing.ALL, signersId, SignerRole.ABSOLUTE, UUID.randomUUID());
            User user3 = new User(0, null, descr.getSigners()[1], null, "Marie");
            User user1 = new User(1, null, UUID.randomUUID(), null, "Marie");
            user3.generateKeyPair(UUID.randomUUID());
            UUID [] signersId1 = {UUID.randomUUID(), UUID.randomUUID()};
            StageDescription last = new StageDescription(null, WayOfStagePassing.ANY, signersId, SignerRole.ABSOLUTE, UUID.randomUUID());
            StageDescription second = new StageDescription(last, WayOfStagePassing.ALL, signersId1, SignerRole.ABSOLUTE, UUID.randomUUID());
            StageDescription first = new StageDescription(second, WayOfStagePassing.ALL, SignerRole.RELATIVE, UUID.randomUUID());
            List<StageDescription> stages = new ArrayList<StageDescription>();
            stages.add(first);
            stages.add(second);
            stages.add(last);
            DocSignStage stage = new DocSignStage(null, descr, id);
            List<DocSignStage> stages1 = new ArrayList<DocSignStage>();
            stages1.add(stage);
            try {
                DocTemplate t = new DocTemplate(tt, 0, "Template 1", stages);
                Document doc2 = new Document("Doc2", tt, startDate, t, user1, stages1, Status.PROCESSING);
                Assert.fail("Constructor failed!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                DocTemplate t = new DocTemplate(tt, 1, "Template 1", stages);
                Document doc2 = new Document("Doc2", tt, startDate, t, user1, stages1, Status.PROCESSING);
                Assert.fail("Constructor failed!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                stages1.add(stage);
                stages1.add(stage);
                DocTemplate t = new DocTemplate(tt, 1, "Template 1", stages);
                Document doc2 = new Document("Doc2", tt, startDate, t, user1, stages1, Status.PROCESSING);
                Assert.fail("Constructor failed!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                 stages = new ArrayList<StageDescription>();
                //stages.add(first);
                stages.add(second);
                stages.add(last);
                 stage = new DocSignStage(null, last, id);
                DocSignStage stage1 = new DocSignStage(stage, second, id);
                stages1 = new ArrayList<DocSignStage>();
                stages1.add(stage);
                stages1.add(stage1);
                DocTemplate t = new DocTemplate(tt, 1, "Template 1", stages);
                Document doc2 = new Document("Doc2", tt, startDate, t, user1, stages1, Status.PROCESSING);
                Assert.fail("Constructor failed!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                stages = new ArrayList<StageDescription>();
                //stages.add(first);
                stages.add(second);
                stages.add(last);
                 stage = new DocSignStage(null, last, id);
                DocSignStage stage2 = new DocSignStage(null, last, id);
                DocSignStage stage1 = new DocSignStage(stage2, second, id);
                stages1 = new ArrayList<DocSignStage>();
                stages1.add(stage1);
                stages1.add(stage);
                DocTemplate t = new DocTemplate(tt, 1, "Template 1", stages);
                Document doc2 = new Document("Doc2", tt, startDate, t, user1, stages1, Status.PROCESSING);
                Assert.fail("Constructor failed!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                stages = new ArrayList<StageDescription>();
                //stages.add(first);
                stages.add(second);
                stages.add(last);
                 stage = new DocSignStage(null, last, id);
                DocSignStage stage1 = new DocSignStage(stage, second, id);
                stages1 = new ArrayList<DocSignStage>();
                stages1.add(stage1);
                stages1.add(stage);
                DocTemplate t = new DocTemplate(tt, 1, "Template 1", stages);
                Document doc2 = new Document("Doc2", tt, startDate, t, user1, stages1, Status.REFUSED);
                Assert.fail("Constructor failed!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                stages = new ArrayList<StageDescription>();
                //stages.add(first);
                stages.add(second);
                stages.add(last);
                 stage = new DocSignStage(null, last, id);
                DocSignStage stage1 = new DocSignStage(stage, second, id);
                stages1 = new ArrayList<DocSignStage>();
                stages1.add(stage1);
                stages1.add(stage);
                DocTemplate t = new DocTemplate(tt, 1, "Template 1", stages);
                Document doc2 = new Document("Doc2", tt, startDate, t, user1, stages1, Status.SIGNED);
                Assert.fail("Constructor failed!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                stages = new ArrayList<StageDescription>();
                //stages.add(first);
                stages.add(second);
                stages.add(last);
                 stage = new DocSignStage(null, last, id);
                DocSignStage stage1 = new DocSignStage(stage, second, id);
                stages1 = new ArrayList<DocSignStage>();
                stages1.add(stage1);
                stages1.add(stage);
                DocTemplate t = new DocTemplate(tt, 1, "Template 1", stages);
                Document doc2 = new Document("Doc2", tt, startDate, t, user1, stages1, Status.PREPARING);
                Assert.assertEquals("Doc2", doc2.getName());
                Assert.assertEquals(tt, doc2.getDoc());
                Assert.assertEquals(null, doc2.getDatePass());
                Assert.assertEquals(user1.getId(), doc2.getCreator().getId());
                Assert.assertEquals(Status.PREPARING.getValue(), doc2.getStatus());
                Assert.assertEquals(false, doc2.checkIfReady());
                //System.out.println(doc2.getCurrentStage());
                Assert.assertEquals(0, doc2.getCurrentStageIndex());
                try {
                    doc2.signDocument("Signed", new User(0, null, signersId[0], null, "Marie"), UUID.randomUUID());
                    Assert.fail("Make sign failed!");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                try {
                    doc2.refuseToSign("Refused", "Refused because", new User(0, null, signersId[0], null, "Marie"), UUID.randomUUID());
                    Assert.fail("Refuse sign failed!");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                Assert.assertEquals(true, doc2.setEnableToSign());
                Assert.assertEquals(false, doc2.setEnableToSign());
                Assert.assertEquals(Status.PROCESSING.getValue(), doc2.getStatus());
                try {
                    doc2.refuseToSign("Refused", "Refused because", new User(0, null, signersId1[0], null, "Marie"), UUID.randomUUID());
                    Assert.fail("Refuse sign failed!");
                } catch (NoActualKeyPairException e) {
                    System.out.println(e.getMessage());
                }
                User user = new User(0, null, signersId1[0], null, "Marie");
                user.generateKeyPair(UUID.randomUUID());
                try {
                    doc2.refuseToSign("Refused", "Refused because", user, UUID.randomUUID());
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    Assert.fail("Refuse sign failed!");
                }
                Assert.assertEquals(Status.REFUSED.getValue(), doc2.getStatus());
                Calendar date = Calendar.getInstance();
                Assert.assertEquals(date, doc2.getDatePass());
                Assert.assertEquals("Refused because", doc2.getCommentOfLastSign());
                try {
                    doc2.refuseToSign("Refused", "Refused because", user, UUID.randomUUID());
                    Assert.fail("Refuse sign failed!");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                Assert.fail("Constructor failed!");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
