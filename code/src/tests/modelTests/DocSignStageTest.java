package tests.modelTests;

import model.*;
import org.junit.Assert;
import org.junit.Test;

import javax.print.Doc;
import java.security.Signature;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class DocSignStageTest {
    @Test

    public void testDocSignStage() {
        try { // empty stage descr
            DocSignStage stage = new DocSignStage(null, null, null, null, null, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        UUID[] signersId = {UUID.randomUUID(), UUID.randomUUID()};
        UUID id = UUID.randomUUID();
        StageDescription descr = new StageDescription(null, WayOfStagePassing.ALL, signersId, SignerRole.ABSOLUTE, UUID.randomUUID());
        Calendar startDate = Calendar.getInstance();
        try { // empty status
            DocSignStage stage = new DocSignStage(null, null, null, descr, null, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try { // empty date
            DocSignStage stage = new DocSignStage(null, null, null, descr, Status.SIGNED, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try { // empty uuid
            DocSignStage stage = new DocSignStage(null, startDate, null, descr, Status.REFUSED, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try { // empty owner
            DocSignStage stage = new DocSignStage(null, startDate, null, descr, Status.REFUSED, id, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try { // signed but no signs
            DocSignStage stage = new DocSignStage(null, null, null, descr, Status.SIGNED, id, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        User owner = new User(0, null, UUID.randomUUID(), null, "Marie");
        try { //refused but no signs
            DocSignStage stage = new DocSignStage(null, startDate, null, descr, Status.SIGNED, id, owner);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        byte[] doc = new byte[] {(byte)0x65, (byte)0x37};
        User user = new User(1, null, UUID.randomUUID(), null, "Marie");
        try {
            user.generateKeyPair(UUID.randomUUID());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("Constructor failed!");
        }
        try {
            Signature rsa = Signature.getInstance("SHA1withRSA");
            rsa.initSign(user.getActualKeyPair().getPrivateKey());
            rsa.update(doc);
            byte [] signValue = rsa.sign();
            Calendar date = Calendar.getInstance();
            DocSign sign = new DocSign("Signed", null, date, false, signValue, user.getActualKeyPair(), id);
            List<DocSign> signs = new ArrayList<DocSign>();
            signs.add(sign);
            User user2 = new User(0, null, descr.getSigners()[0], null, "Marie");
            user2.generateKeyPair(UUID.randomUUID());
            rsa = Signature.getInstance("SHA1withRSA");
            rsa.initSign(user2.getActualKeyPair().getPrivateKey());
            rsa.update(doc);
            byte [] signValue1 = rsa.sign();
            Calendar date2 = Calendar.getInstance();
            DocSign sign1 = new DocSign("Signed", null, date2, false, signValue, user2.getActualKeyPair(), id);
            List<DocSign> signs1 = new ArrayList<DocSign>();
            signs1.add(sign1);
            try {
                DocSignStage stage = new DocSignStage(null, date2, signs, descr, Status.SIGNED, id, owner);
                Assert.fail("Constructor failed7!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            try {
                DocSignStage stage = new DocSignStage(null, date2, signs, descr, Status.REFUSED, id, owner);
                Assert.fail("Constructor failed2!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            UUID[] ids = { UUID.randomUUID()};
            StageDescription descr1 = new StageDescription(null, WayOfStagePassing.ALL, ids, SignerRole.ABSOLUTE, UUID.randomUUID());
            try {
                DocSignStage stage = new DocSignStage(null, null, signs, descr1, Status.PROCESSING, id, owner);
                Assert.fail("Constructor failed6!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            try {
                DocSignStage stage = new DocSignStage(null, null, signs, descr, Status.PROCESSING, id, owner);
                Assert.fail("Constructor failed7!");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            try {
                DocSignStage stage = new DocSignStage(null, null, signs1, descr, Status.PROCESSING, id, owner);
                Assert.assertEquals(null, stage.getNextStage());
                Assert.assertEquals(descr, stage.getDescription());
                Assert.assertEquals(Status.PROCESSING.getValue(), stage.getStatus());
                Assert.assertEquals(id, stage.getId());
                User user3 = new User(0, null, descr.getSigners()[1], null, "Marie");
                user3.generateKeyPair(UUID.randomUUID());
                stage.makeSign("Signed", user, user3, doc, UUID.randomUUID());
                Assert.assertEquals(Status.SIGNED.getValue(), stage.getStatus());
                try {
                    stage.makeSign("Signed", user, user3, doc, UUID.randomUUID());
                    Assert.fail("Make sign faied!");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                // проверка на refused;
                DocSignStage stage2 = new DocSignStage(null, descr, id);
                stage2.refuseSign("Refused", "Refused because...", user, user3, UUID.randomUUID());
                Assert.assertEquals(Status.REFUSED.getValue(), stage2.getStatus());
                try {
                    stage2.makeSign("Refused", user, user3, doc, UUID.randomUUID());
                    Assert.fail("Make sign faied!");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
                Assert.fail("Constructor failed!");
            }

        } catch (Exception e) {
            System.out.println("Constructor failed0!");
            Assert.fail();
        }

    }
}
