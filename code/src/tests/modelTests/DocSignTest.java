package tests.modelTests;

import model.DocSign;
import model.User;
import org.junit.Assert;
import org.junit.Test;

import java.security.Signature;
import java.util.Calendar;
import java.util.UUID;

public class DocSignTest {
    @Test
    public void testDocSign() {

        DocSign sign;
        try { //empty status
            sign = new DocSign("", null, null, false, null, null, null);
            Assert.fail("COnstructor failed");
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        try { //empty date
            sign = new DocSign("Signed", null, null, false, null, null, null);
            Assert.fail("COnstructor failed");
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        Calendar startDate = Calendar.getInstance();
        try { // empty sign value
            sign = new DocSign("Signed", null, startDate, false, null, null, null);
            Assert.fail("COnstructor failed");
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        byte[] signValue = new byte[] {(byte)0x65, (byte)0x37};
        try { // empty key
            sign = new DocSign("Signed", null, startDate, false, signValue, null, null);
            Assert.fail("COnstructor failed");
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        User user = new User(1, null, UUID.randomUUID(), null, "Marie");
        try {
            user.generateKeyPair(UUID.randomUUID());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("Constructor failed!");
        }
        try { // key pair not valid
            sign = new DocSign("Signed", null, startDate, false, signValue, user.getActualKeyPair(), null);
            Assert.fail("COnstructor failed");
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        try { //empty uuid
            sign = new DocSign("Signed", null, user.getActualKeyPair().getStartDate(), true, signValue, user.getActualKeyPair(), null);
            Assert.fail("COnstructor failed");
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        try { // no comment
            sign = new DocSign("Signed", null, user.getActualKeyPair().getStartDate(), true, signValue, user.getActualKeyPair(), UUID.randomUUID());
            Assert.fail("COnstructor failed");
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        byte[] doc = new byte[] {(byte)0x65, (byte)0x37};
        UUID id = UUID.randomUUID();
        try {
            Signature rsa = Signature.getInstance("SHA1withRSA");
            rsa.initSign(user.getActualKeyPair().getPrivateKey());
            rsa.update(doc);
            signValue = rsa.sign();
            sign = new DocSign("Signed", null, user.getActualKeyPair().getEndDate(), false, signValue, user.getActualKeyPair(), id);
            Assert.assertEquals("Signed", sign.getStatus());
            Assert.assertEquals(null, sign.getComment());
            Assert.assertEquals(false, sign.checkIfRefused());
            //Assert.assertEquals(user.getActualKeyPair().getStartDate(),sign.getDateOfSign());
            Assert.assertEquals(true, sign.checkSign(doc));
            Assert.assertEquals(id, sign.getId());
        } catch(Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("COnstructor failed");
        }

        try {
            sign = new DocSign("Signed", doc, user.getActualKeyPair(), id);
            Assert.assertEquals("Signed", sign.getStatus());
            Assert.assertEquals(null, sign.getComment());
            Assert.assertEquals(false, sign.checkIfRefused());
            //Assert.assertEquals(startDate,sign.getDateOfSign());
            Assert.assertEquals(true, sign.checkSign(doc));
            Assert.assertEquals(id, sign.getId());
        } catch(Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("COnstructor failed0");
        }

        try {
            sign = new DocSign("Refused", "Refused because ...", user.getActualKeyPair(), id);
            Assert.assertEquals("Refused", sign.getStatus());
            Assert.assertEquals("Refused because ...", sign.getComment());
            Assert.assertEquals(true, sign.checkIfRefused());
            //Assert.assertEquals(startDate,sign.getDateOfSign());
            Assert.assertEquals(false, sign.checkSign(doc));
            Assert.assertEquals(id, sign.getId());
        } catch(Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("COnstructor failed1");
        }

        try {
            user.getActualKeyPair().setNotActual();
            sign = new DocSign("Refused", "Refused because ...", user.getKeyPairForDate(startDate), id);
            Assert.assertEquals("Refused", sign.getStatus());
            Assert.assertEquals("Refused because ...", sign.getComment());
            Assert.assertEquals(true, sign.checkIfRefused());
            //Assert.assertEquals(startDate,sign.getDateOfSign());
            Assert.assertEquals(false, sign.checkSign(doc));
            Assert.assertEquals(id, sign.getId());
            Assert.fail("Constructor failed2");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

    }

}
