package tests.modelTests;

import model.SignerRole;
import model.StageDescription;
import model.WayOfStagePassing;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class StageDescriptionTest {
    @Test
    public void testStageDescription() {
        try {
            StageDescription first1 = new StageDescription(null, null, SignerRole.RELATIVE, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            StageDescription first1 = new StageDescription(null, WayOfStagePassing.ALL, null, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            StageDescription first1 = new StageDescription(null, WayOfStagePassing.ALL, SignerRole.RELATIVE, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            StageDescription first1 = new StageDescription(null, WayOfStagePassing.ALL, SignerRole.ABSOLUTE, null);
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            StageDescription first1 = new StageDescription(null, WayOfStagePassing.ALL, SignerRole.ABSOLUTE, UUID.randomUUID());
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        UUID[] signersId = {UUID.randomUUID(), UUID.randomUUID()};
        try {
            StageDescription first1 = new StageDescription(null, WayOfStagePassing.ALL, signersId, SignerRole.RELATIVE, UUID.randomUUID());
            Assert.fail("Constructor failed!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            StageDescription first1 = new StageDescription(null, WayOfStagePassing.ALL, signersId, SignerRole.ABSOLUTE, UUID.randomUUID());
            Assert.assertEquals(null, first1.getNextStageDescription());
            Assert.assertEquals(WayOfStagePassing.ALL.getValue(), first1.getWayPass());
            Assert.assertEquals(SignerRole.ABSOLUTE.getValue(), first1.getRole());
            Assert.assertEquals(signersId.length, first1.getSigners().length);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("Constructor failed!");
        }

        try {
            StageDescription first1 = new StageDescription(null, WayOfStagePassing.ALL, SignerRole.RELATIVE, UUID.randomUUID());
            Assert.assertEquals(null, first1.getNextStageDescription());
            Assert.assertEquals(WayOfStagePassing.ALL.getValue(), first1.getWayPass());
            Assert.assertEquals(SignerRole.RELATIVE.getValue(), first1.getRole());
            Assert.assertEquals(null, first1.getSigners());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            Assert.fail("Constructor failed!");
        }
    }
}
